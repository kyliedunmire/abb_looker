view: approval_types {
  sql_table_name: mosql.approval_types ;;


  dimension: approval_type_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."approval_type_id" ;;
  }

  dimension: approval_type {
    type: string
    sql: ${TABLE}."approval_type_label" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  measure: count {
    type: count
    drill_fields: [approval_type_id]
  }
}
