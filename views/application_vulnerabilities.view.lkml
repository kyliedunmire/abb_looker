view: application_vulnerabilities {
  sql_table_name: abb.application_vulnerabilities ;;

  dimension: application_id {
    type: string
    hidden: yes
    sql: ${TABLE}."application_id" ;;
  }

  dimension: vulnerability_id {
    hidden: yes
    type: string
    sql: ${TABLE}."vulnerability_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [applications.application_name, applications.application_id]
  }
}
