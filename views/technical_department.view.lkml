view: technical_department {
  derived_table: {
    sql: SELECT *
    FROM abb.vendor_department
    WHERE vendor_department_type = 'deptTechnical';;
  }

  dimension: technical_department_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.vendor_department_id ;;
  }

  dimension: technical_department {
    group_label: "Business Units"
    type: string
    sql: ${TABLE}.vendor_department_name ;;
  }
}
