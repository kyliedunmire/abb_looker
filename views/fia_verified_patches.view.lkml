view: fia_overview {
  sql_table_name: fia.fia_verified_patches ;;

  dimension: file_name {
    type: string
    sql: ${TABLE}."file_name" ;;
  }

  dimension: overall_risk_profile {
    type: string
    sql: ${TABLE}."overall_risk_profile" ;;
  }

  dimension: patch {
    type: string
    sql: ${TABLE}."patch" ;;
  }

  dimension: product {
    type: string
    sql: ${TABLE}."product" ;;
  }

  dimension_group: registered {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."registered_date" ;;
  }

  dimension_group: validated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."validated_date" ;;
  }

  dimension: validation_count {
    type: number
    sql: ${TABLE}."validation_count" ;;
  }

  dimension: vendor {
    type: string
    sql: ${TABLE}."vendor" ;;
  }

  dimension: verified {
    type: yesno
    sql: ${TABLE}."verified" ;;
  }

  ######################################################################

  measure: count {
    type: count
    drill_fields: [file_name]
  }

  measure: vendor_count {
    type: count_distinct
    sql: ${vendor} ;;
    drill_fields: [details*]
  }

  measure: product_count {
    type: count_distinct
    sql: ${product} ;;
    drill_fields: [details*]
  }

  measure: files_count {
    type: count_distinct
    sql: ${file_name} ;;
    drill_fields: [details*]
  }

  measure: file_verified {
    type: count
    filters: {
      field: verified
      value: "yes"
    }
    drill_fields: [details*]
  }

  # ----- Sets of fields for drilling ------
  set: details {
    fields: [
      file_name,
      vendor,
      product,
      patch,
      verified,
      registered_date,
      validated_date,
      validation_count,
      overall_risk_profile
    ]
  }
}
