view: assessment_workflow {
  derived_table: {
    sql: SELECT *
      FROM abb.workflow
      WHERE task = 'assessments'
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: start_date {
    type: list
    list_field: step_started_date
  }

  measure: started_by {
    type: list
    list_field: started_user
  }

  measure: complete_date {
    type: list
    list_field: step_completed_date
  }

  measure: completed_by {
    type: list
    list_field: completed_user
  }

  measure: assigned_to {
    type: list
    list_field: assignee
  }

  measure: days_to_complete {
    type: list
    list_field: days_to_complete_step
  }


  dimension: workflow_pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."workflow_pk" ;;
  }

  dimension: object_id {
    hidden: yes
    type: string
    sql: ${TABLE}."object_id" ;;
  }

  dimension: status_id {
    hidden: yes
    type: string
    sql: ${TABLE}."status_id" ;;
  }

  dimension: step {
    type: string
    sql: ${TABLE}."step" ;;
  }

  dimension: workflow_step {
    type: string
    sql: ${TABLE}."workflow_step" ;;
  }

  dimension: date {
    type: date
    sql: ${TABLE}."date" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension: task {
    type: string
    sql: ${TABLE}."task" ;;
  }

  dimension: user_id {
    hidden: yes
    type: string
    sql: ${TABLE}."user_id" ;;
  }

  dimension: days_to_complete_step {
    type: number
    sql: ${TABLE}."days_to_complete" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension: task_due_date {
    type: date
    sql: ${TABLE}."task_due_date" ;;
  }

  dimension: est_completed_date {
    type: date
    sql: ${TABLE}."est_completed_date" ;;
  }

  dimension: step_assigned_to {
    hidden: yes
    type: string
    sql: ${TABLE}."step_assigned_to" ;;
  }

  dimension_group: step_due {
    type: time
    sql: ${TABLE}."step_due_date" ;;
    timeframes: [
      raw,
      date,
      week,
      month,
      year
    ]
  }

  dimension: tag {
    hidden: yes
    type: string
    sql: ${TABLE}."tag" ;;
  }

  dimension: phase {
    type: string
    sql: ${TABLE}.phase ;;
  }

  dimension: assignee_id {
    hidden: yes
    type: string
    sql: ${TABLE}.assignee ;;
  }

  dimension: assignee {
    type: string
    sql: ${assignee.full_name} ;;
  }

  dimension: step_user_id {
    hidden: yes
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: step_user {
    type: string
    sql: ${step_user.full_name} ;;
  }

  dimension: started_user {
    type: string
    sql: CASE
          WHEN ${step} = 'start'
            THEN ${step_user}
          ELSE NULL
          END;;
  }

  dimension: step_started_date {
    type: date
    sql: CASE
          WHEN ${step} = 'start'
          THEN ${date}
          ELSE NULL
          END;;
  }

  dimension: completed_user {
    type: string
    sql: CASE
          WHEN ${step} = 'complete'
            THEN ${step_user}
          ELSE NULL
          END;;
  }

  dimension: step_completed_date {
    type: date
    sql: CASE
          WHEN ${step} = 'complete'
          THEN ${date}
          ELSE NULL
          END;;
  }



  set: detail {
    fields: [
      assessments.name,
      assessments.description,
      assessment_assigned_to.full_name,
      assessments.due_date,
      step_started_date,
      step_completed_date,
      assessment_workflow_sla.days_past_due,
      step_assigned_to
    ]
  }
}
