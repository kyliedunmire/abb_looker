view: asset_vulnerabilities {
  sql_table_name: abb.asset_vulnerabilities ;;

  dimension: asset_id {
    type: string
    hidden: yes
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: vulnerability_id {
    type: string
    sql: ${TABLE}."vulnerability_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [assets.asset_name, assets.asset_id]
  }
}
