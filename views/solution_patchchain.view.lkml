view: solution_patchchain {
  derived_table: {
    sql: SELECT sol.solution_id,
            sol.solution_name,
            sq.question_text,
            sa.answer_text,
            soc.comment,
            so.survey_instance_id,
            sq.qid                                       as question_number,
            sq.section_title                             as title,
            sq.question_weight,
            sa.answer_weight,
            (sq.question_weight * sa.answer_weight)::int as score,
       app.application_id,
       assets.asset_id
            FROM abb.solutions sol
              INNER JOIN abb.survey_outcomes_internal so
                        ON so.survey_outcome_id = sol.solution_id AND
                           (so.survey_template_id = 'gbwXimMrrB6dw8wPF')
              LEFT JOIN (
                    SELECT DISTINCT ON (question_id) question_id,
                                          section_title,
                                          qid,
                                          question_text,
                                          question_weight,
                                          section_index,
                                          bo_id,
                                          survey_instance_id
                    FROM abb.survey_object_internal) sq ON sq.question_id::text = so.question_id::text
              LEFT JOIN (
                    SELECT answer_id, answer_text, answer_weight, question_id, survey_instance_id
                    FROM abb.survey_object_internal) sa ON so.answer_id::text = sa.answer_id::text AND sq.question_id = sa.question_id AND sq.survey_instance_id = sa.survey_instance_id
              LEFT JOIN (
                    SELECT *
                    FROM abb.survey_outcome_comments_internal) soc ON soc.question_id = sq.question_id
LEFT JOIN abb.application_solutions appsol ON sol.solution_id = appsol.solution_id
LEFT JOIN abb.applications app ON appsol.application_id = app.application_id
LEFT JOIN abb.asset_solutions asssol ON sol.solution_id = asssol.solution_id
LEFT JOIN abb.assets ON asssol.asset_id = assets.asset_id
            ORDER BY sq.section_index, question_number
 ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: count_distinct_patches {
    type: count_distinct
    sql: ${solution_id} ;;
  }

  measure: count_distinct_assets_and_applications {
    type: count_distinct
    sql: ${object_id} ;;
    drill_fields: [detail*]
  }

  ###################################################################################################

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${solution_id} || ${question_text} ;;
  }

  dimension: solution_id {
    type: string
    sql: ${TABLE}."solution_id" ;;
  }

  dimension: solution_name {
    type: string
    sql: ${TABLE}."solution_name" ;;
  }

  dimension: question_text {
    type: string
    sql: ${TABLE}."question_text" ;;
  }

  dimension: answer_text {
    type: string
    sql: ${TABLE}."answer_text" ;;
  }

  dimension: comment {
    type: string
    sql: ${TABLE}."comment" ;;
  }

  dimension: survey_instance_id {
    type: string
    sql: ${TABLE}."survey_instance_id" ;;
  }

  dimension: question_number {
    type: string
    sql: ${TABLE}."question_number" ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}."title" ;;
  }

  dimension: question_weight {
    type: number
    sql: ${TABLE}."question_weight" ;;
  }

  dimension: answer_weight {
    type: number
    sql: ${TABLE}."answer_weight" ;;
  }

  dimension: score {
    type: number
    sql: ${TABLE}."score" ;;
  }

  dimension: application_id {
    type: string
    sql: ${TABLE}."application_id" ;;
  }

  dimension: asset_id {
    type: string
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: object_id {
    hidden: yes
    type: string
    sql: CASE
          WHEN ${application_id} IS NOT NULL
          THEN ${application_id}
          WHEN ${asset_id} IS NOT NULL
          THEN ${asset_id}
          ELSE NULL
          END;;
  }

  set: detail {
    fields: [
      solution_id,
      solution_name,
      question_text,
      answer_text,
      comment,
      survey_instance_id,
      question_number,
      title,
      question_weight,
      answer_weight,
      score,
      application_id,
      asset_id
    ]
  }
}
