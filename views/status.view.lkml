view: status {
  sql_table_name: abb.status ;;
  drill_fields: [status_id]

  dimension: status_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."status_id" ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}."status_label" ;;
  }

  dimension: status_type {
    type: string
    sql: ${TABLE}."status_type" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      status_id,
      applications.count,
      assessment_findings.count,
      assessments.count,
      assets.count,
      detected_vulnerabilities.count,
      engagement_scopes.count,
      risk_outcomes.count,
      services.count,
      survey_instances.count,
      tasks.count,
      vulnerabilities.count,
      workflow.count
    ]
  }
}
