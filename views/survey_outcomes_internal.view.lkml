view: survey_outcomes_internal {
  sql_table_name: abb.survey_outcomes_internal ;;

  dimension: answer_id {
    hidden: yes
    type: string
    sql: ${TABLE}."answer_id" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: created_by {
    group_label: "Created By"
    group_item_label: "Name"
    type: string
    sql: ${survey_outcome_created.full_name} ;;
  }

  dimension: created_by_email {
    group_label: "Created By"
    group_item_label: "Email"
    type: string
    sql: ${survey_outcome_created.email} ;;
  }

  dimension: freeform_answer {
    type: string
    sql: ${TABLE}."freeform_answer" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: question_id {
    hidden: yes
    type: string
    sql: ${TABLE}."question_id" ;;
  }

  dimension: status {
    hidden: yes
    type: string
    sql: ${TABLE}."status" ;;
  }

  dimension: survey_instance_id {
    type: string
    hidden: yes
    sql: ${TABLE}."survey_instance_id" ;;
  }

  dimension: survey_outcome_id {
    hidden: yes
    type: string
    sql: ${TABLE}."survey_outcome_id" ;;
  }

  dimension: survey_template_id {
    hidden: yes
    type: string
    sql: ${TABLE}."survey_template_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [survey_instances.survey_instance_id]
  }
}
