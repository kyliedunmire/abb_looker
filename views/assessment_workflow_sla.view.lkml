view: assessment_workflow_sla_test {
  derived_table: {
    sql: SELECT workflow_pk,
       object_id,
       phase,
       workflow_step,
       days_to_complete,
       step_assigned_to,
       task_due_date,
       start_date,
       complete_date,
       step_due_date,
       CASE
           WHEN complete_date IS NOT NULL
               THEN sum(CASE WHEN extract(dow from complete_date_series) IN (1, 2, 3, 4, 5) THEN 1 ELSE 0 END)
           ELSE sum(CASE WHEN extract(dow from now_date_series) IN (1, 2, 3, 4, 5) THEN 1 ELSE 0 END)
           END as days_from_start_to_complete
FROM (
         WITH start as (
             SELECT *
             FROM abb.workflow
             WHERE step = 'start'
         ),
              complete as (
                  SELECT *
                  FROM abb.workflow
                  WHERE step = 'complete'
              )
         SELECT start.workflow_pk,
                start.object_id,
                start.phase,
                start.workflow_step,
                start.days_to_complete,
                start.step_assigned_to,
                start.task_due_date,
                start.date                                                    as start_date,
                complete.date                                                 as complete_date,
                start.step_due_date,
                generate_series(start.date, complete.date, '1 day'::interval) as complete_date_series,
                generate_series(start.date, NOW(), '1 day'::interval)         as now_date_series
         FROM start
                  INNER JOIN complete ON start.object_id = complete.object_id AND start.phase = complete.phase AND
                                         start.workflow_step = complete.workflow_step) as bs
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
 ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: overdue_count {
    type: count
    filters: {
      field: is_past_due
      value: "Yes"
    }
  }

  measure: workflow_steps_count {
    type: count
    drill_fields: [detail*]
  }

  measure: average_days_to_complete {
    type: average
    sql: ${days_from_start} ;;
    value_format_name: decimal_0
    drill_fields: [detail*]
  }

########################################################################################################

  dimension: workflow_pk {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}."workflow_pk" ;;
  }

  dimension: object_id {
    hidden: yes
    type: string
    sql: ${TABLE}."object_id" ;;
  }

  dimension: phase {
    type: string
    sql: ${TABLE}."phase" ;;
  }

  dimension: workflow_step {
#     hidden: yes
    type: string
    sql: ${TABLE}."workflow_step" ;;
  }

#   dimension: workflow_step {
#     type: string
#     case: {
#       when: {
#         sql: ${unordered_workflow_step} = 'SIG Sent' ;;
#         label: "SIG Sent"
#       }
#       when: {
#         sql: ${unordered_workflow_step} = 'SIG Received' ;;
#         label: "SIG Received"
#       }
#       when: {
#         sql: ${unordered_workflow_step} = 'SIG Reviewed' ;;
#         label: "SIG Reviewed"
#       }
#       when: {
#         sql: ${unordered_workflow_step} = 'Preliminary Findings Call' ;;
#         label: "Preliminary Findings Call"
#       }
#       when: {
#         sql: ${unordered_workflow_step} = 'SIG Final' ;;
#         label: "SIG Final"
#       }
#     }
#   }

  dimension: days_allowed {
    type: number
    sql: ${TABLE}."days_to_complete" ;;
  }

  dimension: step_assigned_to {
    hidden: yes
    type: string
    sql: ${TABLE}."step_assigned_to" ;;
  }

  dimension: step_assignee {
    type: string
    sql: ${assignee.full_name} ;;
  }

  dimension: task_due_date {
    type: date
    sql: ${TABLE}."task_due_date" ;;
  }

  dimension: start_date {
    type: date
    sql: ${TABLE}."start_date" ;;
  }

  dimension: complete_date {
    type: date
    sql: ${TABLE}."complete_date" ;;
  }

  dimension: days_from_start {
    type: number
    sql: ${TABLE}."days_from_start_to_complete" ;;
    description: "If the step is complete, the calculated business days from the start date to the complete date. If incomplete, the calculated business days from the start date to today."
  }

  dimension: difference_between_sla_and_actual {
    type: number
    sql: ${days_allowed} - ${days_from_start}  ;;
  }

  dimension: is_past_due {
    type: yesno
    sql: CASE
          WHEN ${days_from_start} > ${days_allowed}
          THEN TRUE
          ELSE FALSE
          END;;
  }

  dimension: step_due_date {
    type: date
    sql: ${TABLE}.step_due_date ;;
  }


####################################################################################################################

  set: detail {
    fields: [
      assessments.platform_id,
      assessments.name,
      phase,
      workflow_step,
      step_assignee,
      days_allowed,
      task_due_date,
      start_date,
      complete_date,
      days_from_start,
      average_days_to_complete
    ]
  }
}
