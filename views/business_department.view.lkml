view: business_department {
  sql_table_name: abb.business_department ;;

  dimension: business_department_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.business_department_id ;;
  }

  dimension: business_department {
    group_label: "Business Units"
    type: string
    sql: ${TABLE}.business_department_name ;;
  }
}
