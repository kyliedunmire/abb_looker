view: vendor_department {
  sql_table_name: abb.vendor_department ;;
  drill_fields: [vendor_department_id]

  dimension: vendor_department_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."vendor_department_id" ;;
  }

  dimension: vendor_department_name {
    type: string
    sql: ${TABLE}."vendor_department_name" ;;
  }

  dimension: vendor_department_type {
    type: string
    sql: ${TABLE}."vendor_department_type" ;;
  }

  measure: count {
    type: count
    drill_fields: [vendor_department_id, vendor_department_name]
  }
}
