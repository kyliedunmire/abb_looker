view: engagement_scopes {
  sql_table_name: abb.engagement_scopes ;;
  drill_fields: [engagement_scope_id]

  dimension: engagement_scope_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."engagement_scope_id" ;;
  }

  dimension: application_id {
    type: string
    hidden: yes
    sql: ${TABLE}."application_id" ;;
  }

  dimension: asset_id {
    type: string
    hidden: yes
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}."class_id" ;;
  }

  dimension: contract_id {
    hidden: yes
    type: string
    sql: ${TABLE}."contract_id" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."created_by" ;;
  }

#   dimension: created_by {
#     group_label: "Created By"
#     group_item_label: "Name"
#     type: string
#     sql: ${engagement_created.full_name} ;;
#   }

#   dimension: created_by_email {
#     group_label: "Created By"
#     group_item_label: "Email"
#     type: string
#     sql: ${engagement_created.email} ;;
#   }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."engagement_scope_name" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."engagement_type" ;;
  }

  dimension: resolution_id {
    type: string
    hidden: yes
    sql: ${TABLE}."resolution_id" ;;
  }

  dimension: severity_id {
    type: string
    hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: vendor_id {
    type: string
    hidden: yes
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}.platform_id ;;
  }

  ####--------------------------------------------------------------------------------------------------------------------------------------------

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      platform_id,
      name,
      vendors.vendor_name,
      procurement_status.status,
      procurement_severity.severity
    ]
  }
}
