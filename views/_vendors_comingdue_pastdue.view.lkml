view: _vendors_comingdue_pastdue {
  sql_table_name: abb.vendors ;;

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: count_distinct {
    type: count_distinct
    sql: ${vendor_name} ;;
  }


#   dimension: vendor_pk {
#     primary_key: yes
#     hidden: yes
#     type: string
#     sql: ${TABLE}."vendor_pk" ;;
#   }

  dimension: vendor_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: vendor_name {
    type: string
    sql: ${TABLE}."vendor_name" ;;
#     link: {
#       label: "Open new tab of Vendor page"
#       url: "https://southern-platform.com/vendors/details/{{ vendors.vendor_id }}"
#     }
    html: <a href="https://fis-mtn.fortressis.com/vendors/details/{{ all_objects.vendor_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: duns_number {
    type: string
    sql: ${TABLE}."duns_number" ;;
  }

  dimension: address {
    type: string
    sql: ${TABLE}."address" ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}."city" ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}."state" ;;
  }

  dimension: postal_code {
    type: string
    sql: ${TABLE}."postal_code" ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension: severity_id {
    hidden: yes
    type: string
    sql: ${TABLE}.severity_id ;;
  }

  dimension: severity {
    hidden: yes
    type: string
    sql: ${vendor_severity.severity} ;;
  }

  dimension: criticality {
    type: string
    case: {
      when: {
        sql: ${severity} = 'Critical' ;;
        label: "Critical"
      }
      when: {
        sql: ${severity} = 'High' ;;
        label: "High"
      }
      when: {
        sql: ${severity} = 'Moderate' ;;
        label: "Moderate"
      }
      when: {
        sql: ${severity} = 'Low' ;;
        label: "Low"
      }
      when: {
        sql: ${severity} IN ('Nominal','Not Ranked') ;;
        label: "Nominal"
      }
    }
  }

  dimension: type {
    type: string
    sql: ${TABLE}.vendor_type ;;
  }

  dimension: org_id {
    hidden: yes
    type: number
    sql: ${TABLE}.org_id::int ;;
  }

  dimension: comingdue_pastdue {
    label: "Coming Due/Past Due"
    type: string
    sql: CASE
          WHEN ${services.coming_due_past_due_general} = 'Coming due' OR ${assessments.coming_due_past_due_general} = 'Coming due' OR ${assessment_findings.coming_due_past_due_general} = 'Coming due'
          THEN 'Coming due'
          WHEN ${services.coming_due_past_due_general} = 'Past due' OR ${assessments.coming_due_past_due_general} = 'Past due' OR ${assessment_findings.coming_due_past_due_general} = 'Past due'
          THEN 'Past due'
          WHEN ${services.coming_due_past_due_general} = 'Complete' OR ${assessments.coming_due_past_due_general} = 'Complete' OR ${assessment_findings.coming_due_past_due_general} = 'Complete'
          THEN 'Complete'
          ELSE 'No Escalation'
          END;;
  }

  dimension: missing_ranking {
    type: yesno
    sql: CASE
          WHEN ${services.name} IS NULL
          THEN TRUE
          ELSE FALSE
          END;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}.class_id ;;
  }


  set: detail {
    fields: [
      vendor_name,
      criticality,
      type,
      description
    ]
  }
}
