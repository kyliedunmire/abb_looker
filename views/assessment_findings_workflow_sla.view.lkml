view: assessment_findings_workflow_sla {
  derived_table: {
    sql: WITH start as(
          SELECT workflow_pk,object_id, phase, workflow_step, date, days_to_complete, step_assigned_to
          FROM abb.workflow
          WHERE step = 'start'
           ),
           complete as (
               SELECT object_id, phase, workflow_step, date
          FROM abb.workflow
          WHERE step = 'complete'
           )
      SELECT start.workflow_pk,
      start.object_id,
      start.phase,
      start.workflow_step,
      start.days_to_complete,
      start.date as start_date,
      complete.date as complete_date,
      start.step_assigned_to as assigned_id
      FROM start
      INNER JOIN complete ON start.object_id = complete.object_id AND start.phase = complete.phase AND start.workflow_step=complete.workflow_step ;;
  }

  measure: overdue_count {
    type: count
    filters: {
      field: is_past_due
      value: "Yes"
    }
  }

  measure: workflow_steps_count {
    type: count
    drill_fields: [detail*]
  }

  measure: average_days_to_complete {
    type: average
    sql: ${days_from_start_to_complete} ;;
    value_format_name: decimal_0
    drill_fields: [detail*]
  }

  #####################################################################################

  dimension: workflow_pk {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.workflow_pk ;;
  }

#   dimension: primary_key {
#     primary_key: yes
#     hidden: yes
#     type: string
#     sql: ${object_id}||${phase}||${workflow_step} ;;
#   }

  dimension: object_id {
    hidden: yes
    type: string
    sql: ${TABLE}.object_id ;;
  }

  dimension: phase {
    type: string
    sql: ${TABLE}.phase ;;
  }

  dimension: workflow_step {
    type: string
    sql: ${TABLE}.workflow_step ;;
  }

  dimension: days_since_start {
    type: number
    sql: CASE
          WHEN ${complete_date} IS NOT NULL
          THEN ${days_from_start_to_complete}
          ELSE (now())::date - ${start_date}
          END;;
  }

  dimension: days_to_complete {
    type: number
    sql: ${TABLE}.days_to_complete ;;
  }

  dimension: start_date {
    type: date
    sql: ${TABLE}.start_date ;;
  }

  dimension: due_date {
    type: date
    sql: ${start_date} + ${days_to_complete} ;;
  }

  dimension: complete_date {
    type: date
    sql: ${TABLE}.complete_date ;;
  }

  dimension: is_past_due {
    type: yesno
    sql: CASE
          WHEN ${days_since_start} > ${days_to_complete}
          THEN TRUE
          ELSE FALSE
          END;;
  }


  dimension: days_past_due {
    type: number
    sql: CASE
          WHEN ${is_past_due} IS TRUE
          THEN ${days_since_start} - ${days_to_complete}
          ELSE NULL
          END;;
  }

  dimension: assigned_id {
    hidden: yes
    type: string
    sql: ${TABLE}.assigned_id ;;
  }

  dimension: step_assignee  {
    type: string
    sql: ${assignee.full_name} ;;
  }

  dimension: days_from_start_to_complete {
    type: number
    sql: ${complete_date} - ${start_date} ;;
  }



  set: detail {
    fields: [
      assessment_findings.name,
      workflow_step,
      assessment_assigned_to.full_name,
      assessment_findings.due_date,
      start_date,
      complete_date,
      days_past_due,
      step_assignee,
      average_days_to_complete
    ]
  }

}
