view: services {
  sql_table_name: abb.services ;;

  dimension: service_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."service_id" ;;
  }

  dimension: assigned_to_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: assigned_to {
    group_label: "Assigned To"
    group_item_label: "Name"
    type: string
    sql: ${service_assigned.full_name} ;;
  }

  dimension: assigned_to_email {
    group_label: "Assigned To"
    group_item_label: "Email"
    type: string
    sql: ${service_assigned.email} ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: created_by {
    group_label: "Created By"
    group_item_label: "Name"
    type: string
    sql: ${service_created.full_name} ;;
  }

  dimension: created_by_email {
    group_label: "Created By"
    group_item_label: "Email"
    type: string
    sql: ${service_created.email} ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."service_name" ;;
    html: <a href="https://fis-mtn-looker.fortressis.com/services/details/{{ services.service_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: severity_id {
    type: string
    hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: submitted_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."submitted_by" ;;
  }

  dimension: submitted_by {
    group_label: "Submitted By"
    group_item_label: "Name"
    type: string
    sql: ${service_submitted.full_name} ;;
  }

  dimension: submitted_by_email {
    group_label: "Submitted By"
    group_item_label: "Email"
    type: string
    sql: ${service_submitted.email} ;;
  }

  dimension_group: submitted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."submitted_date" ;;
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}."tags" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: updated_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."updated_by" ;;
  }

  dimension: updated_by {
    group_label: "Updated By"
    group_item_label: "Name"
    type: string
    sql: ${service_updated.full_name} ;;
  }

  dimension: updated_by_email {
    group_label: "Updated By"
    group_item_label: "Email"
    type: string
    sql: ${service_updated.email} ;;
  }

  dimension: vendor_id {
    type: string
    hidden: yes
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: engagement_scope_id {
    hidden: yes
    type: string
    sql: ${TABLE}.engagement_scope_id ;;
  }

  dimension: process_id {
    hidden: yes
    type: string
    sql: ${TABLE}.process_id ;;
  }

  dimension: coming_due_past_due_general {
    label: "Coming Due/Past Due (General)"
    group_label: "Coming Due/Past Due"
    group_item_label: "General"
    type: string
    sql: CASE
         WHEN ${status_id} = 'maKDXHjoQbw3CS5Ru'
         THEN 'Complete'
         WHEN extract(days from (${due_date} - now())) < 0
         THEN 'Past due'
         WHEN extract(days from (${due_date} - now())) < 7
         THEN 'Coming due'
         ELSE 'No escalation required'
         END;; #There are 2 assessment finding status documents, bringing in the back end ID of both just in case
    description: "Generalizing all incomplete Assessments as either past due or coming due within 2 days"
  }

  dimension: coming_due_past_due_detailed {
    label: "Coming Due/Past Due (Detailed)"
    group_label: "Coming Due/Past Due"
    group_item_label: "Detailed"
    type: string
    sql: CASE
          WHEN ${status_id} = 'maKDXHjoQbw3CS5Ru'
          THEN 'Complete'
          WHEN extract(days from (${due_date} - now())) < -6
          THEN '>7 days past due'
          WHEN extract(days from (${due_date} - now())) < 0
          THEN '<7 days past due'
          WHEN extract(days from (${due_date} - now())) < 7
          THEN '<7 days coming due'
          ELSE '>7 days coming due'
          END;; #There are 2 assessment finding status documents, bringing in the back end ID of both just in case
    description: "Providing details about the state of coming due/past due - More than a week past due, within a week past due, coming due within a week, coming due within more than a week."
  }

  ###-------------------------------------------------------------------------------------------

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      platform_id,
      name
    ]
  }
}
