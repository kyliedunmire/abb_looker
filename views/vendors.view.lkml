view: vendors {
  sql_table_name: abb.vendors ;;
  drill_fields: [vendor_id]

  dimension: vendor_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: address {
    type: string
    sql: ${TABLE}."address" ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: replace(replace(replace(${TABLE}."business_department"::varchar(500),'[',''),'"',''),']','') ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}."city" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension: postal_code {
    type: string
    sql: ${TABLE}."postal_code" ;;
  }

  dimension: severity_id {
    type: string
    hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}."state" ;;
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."vendor_name" ;;
    html: <a href="https://fis-mtn.fortressis.com/vendors/details/{{ vendors.vendor_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."vendor_type" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}.platform_id ;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}.class_id ;;
  }

  #########----------------------------------------------------------------------------------

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: total {
    type: count_distinct
    sql: ${vendor_id} ;;
    drill_fields: [platform_id,
      name,
      type,
      vendor_criticality.criticality]
  }

  measure: assessed {
    type: count_distinct
    sql: ${assessments.vendor_id} ;;
    drill_fields: [platform_id,
      name,
      type,
      vendor_criticality.criticality,
      assessments.name, assessments.assessment_type]
  }


  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      platform_id,
      name,
      type,
      vendor_criticality.criticality
    ]
  }
}
