view: workflow_complete {
  derived_table: {
    sql: SELECT DISTINCT on (object_id) * FROM abb.workflow
      WHERE step = 'complete' AND is_final_step IS TRUE;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: workflow_pk {
    hidden: yes
    type: string
    sql: ${TABLE}."workflow_pk" ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: step {
    type: string
    sql: ${TABLE}."step" ;;
  }

  dimension: workflow_step {
    type: string
    sql: ${TABLE}."workflow_step" ;;
  }

  dimension_group: date {
    type: time
    sql: ${TABLE}."date" ;;
  }

  dimension: task {
    type: string
    sql: ${TABLE}."task" ;;
  }

  dimension: user_id {
    type: string
    hidden: yes
    sql: ${TABLE}."user_id" ;;
  }

  dimension: days_to_complete {
    type: number
    sql: ${TABLE}."days_to_complete" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension: object_id {
    type: string
    hidden: yes
    sql: ${TABLE}."object_id" ;;
  }

  dimension_group: task_due_date {
    type: time
    sql: ${TABLE}."task_due_date" ;;
  }

  dimension: step_assigned_to {
    type: string
    hidden: yes
    sql: ${TABLE}."step_assigned_to" ;;
  }

  dimension_group: step_due_date {
    type: time
    sql: ${TABLE}."step_due_date" ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}."tags" ;;
  }

  dimension: phase {
    type: string
    sql: ${TABLE}."phase" ;;
  }

  dimension: assignee {
    type: string
    hidden: yes
    sql: ${TABLE}."assignee" ;;
  }

  dimension: step_user {
    type: string
    hidden: yes
    sql: ${TABLE}."step_user" ;;
  }

  dimension: phase_sort {
    type: number
    hidden: yes
    sql: ${TABLE}."phase_sort" ;;
  }

  dimension: step_sort {
    type: number
    hidden: yes
    sql: ${TABLE}."step_sort" ;;
  }

  dimension: step_performance {
    type: number
    sql: ${TABLE}."step_performance" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension: subscription_id {
    type: string
    hidden: yes
    sql: ${TABLE}."subscription_id" ;;
  }

  set: detail {
    fields: [
      workflow_pk,
      status_id,
      step,
      workflow_step,
      date_time,
      task,
      user_id,
      days_to_complete,
      type,
      object_id,
      task_due_date_time,
      step_assigned_to,
      step_due_date_time,
      tags,
      phase,
      assignee,
      step_user,
      phase_sort,
      step_sort,
      step_performance,
      description,
      subscription_id
    ]
  }
}
