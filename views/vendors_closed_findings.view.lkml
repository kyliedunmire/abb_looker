view: vendors_closed_findings {
  derived_table: {
    sql: SELECT v.vendor_id,
       CASE
           WHEN count(assessment_findings_id) FILTER ( WHERE afs.status_label = 'Closed' ) > 0
               THEN FALSE
           ELSE TRUE
           END AS no_closed_findings,
       CASE
           WHEN count(assessment_findings_id)
                filter ( where afr.resolution_label IN ('Unresolved', 'Reoccurring') AND afs.status_label = 'Closed' ) > 0
               THEN TRUE
           ELSE FALSE
           END AS uncooperative
FROM abb.vendors v
         INNER JOIN abb.assessment_findings af ON v.vendor_id = af.vendor_id
         INNER JOIN abb.status afs ON af.status_id = afs.status_id
         LEFT JOIN abb.resolutions afr ON af.resolution = afr.resolution_id
GROUP BY 1 ;;
  }

  dimension: vendor_id {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.vendor_id ;;
  }

  dimension: no_closed_findings {
    type: yesno
    sql: ${TABLE}.no_closed_findings ;;
  }

  dimension: uncooperative_vendor {
    type: yesno
    sql: ${TABLE}.uncooperative ;;
  }
}
