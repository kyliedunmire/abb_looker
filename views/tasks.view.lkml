view: tasks {
  sql_table_name: abb.tasks ;;
  drill_fields: [task_id]

  dimension: task_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."task_id" ;;
  }

  dimension: assigned_to {
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: risk_outcome_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."risk_outcome_id" ;;
  }

  dimension: severity_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: submitted_by {
    type: string
    sql: ${TABLE}."submitted_by" ;;
  }

  dimension_group: submitted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."submitted_date" ;;
  }

  dimension: task_name {
    type: string
    sql: ${TABLE}."task_name" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}."updated_by" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      task_id,
      task_name,
      status.status_id,
      severity.severity_id,
      risk_outcomes.risk_outcome_id,
      risk_outcomes.outcome_name
    ]
  }
}
