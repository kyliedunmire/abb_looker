view: workflow_sla_totals {
  derived_table: {
    sql: WITH workflow_totals as (SELECT workflow_pk,
       object_id,
       phase,
       workflow_step,
       days_to_complete,
       step_assigned_to,
       task_due_date,
       start_date,
       complete_date,
       step_due_date,
       CASE
           WHEN complete_date IS NOT NULL
               THEN sum(CASE WHEN extract(dow from complete_date_series) IN (1, 2, 3, 4, 5) THEN 1 ELSE 0 END)
           ELSE sum(CASE WHEN extract(dow from now_date_series) IN (1, 2, 3, 4, 5) THEN 1 ELSE 0 END)
           END as days_from_start_to_complete
FROM (
         WITH start as (
             SELECT *
             FROM abb.workflow
             WHERE step = 'start'
         ),
              complete as (
                  SELECT *
                  FROM abb.workflow
                  WHERE step = 'complete'
              )
         SELECT start.workflow_pk,
                start.object_id,
                start.phase,
                start.workflow_step,
                start.days_to_complete,
                start.step_assigned_to,
                start.task_due_date,
                start.date                                                    as start_date,
                complete.date                                                 as complete_date,
                start.step_due_date,
                generate_series(start.date, complete.date, '1 day'::interval) as complete_date_series,
                generate_series(start.date, NOW(), '1 day'::interval)         as now_date_series
         FROM start
                  INNER JOIN complete ON start.object_id = complete.object_id AND start.phase = complete.phase AND
                                         start.workflow_step = complete.workflow_step) as bs
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
SELECT object_id, workflow_pk ,sum(days_from_start_to_complete) as days_to_complete
FROM workflow_totals
GROUP BY 1,2
 ;;
  }

  measure: average_days_to_complete_total_workflow {
    type: average
    sql: ${days_to_complete} ;;
    value_format_name: decimal_0
  }

  dimension: workflow_pk {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.workflow_pk ;;
  }

  dimension: object_id {
    type: string
    sql: ${TABLE}."object_id" ;;
  }

  dimension: days_to_complete {
    type: number
    sql: ${TABLE}."days_to_complete" ;;
  }

}
