view: detected_vulnerabilities {
  sql_table_name: abb.detected_vulnerabilities ;;
  drill_fields: [detected_vulnerability_id]

  dimension: detected_vulnerability_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."detected_vulnerability_id" ;;
  }

  dimension: access_complexity {
    type: string
    sql: ${TABLE}."access_complexity" ;;
  }

  dimension: assigned_to {
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: attack_vector {
    type: string
    sql: ${TABLE}."attack_vector" ;;
  }

  dimension: authentication {
    type: string
    sql: ${TABLE}."authentication" ;;
  }

  dimension: availability_impact {
    type: string
    sql: ${TABLE}."availability_impact" ;;
  }

  dimension: awareness {
    type: string
    sql: ${TABLE}."awareness" ;;
  }

  dimension: business_department {
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension_group: closed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."closed_date" ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}."code" ;;
  }

  dimension: confidentiality_impact {
    type: string
    sql: ${TABLE}."confidentiality_impact" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: cvss {
    type: string
    sql: ${TABLE}."cvss" ;;
  }

  dimension: cwe {
    type: string
    sql: ${TABLE}."cwe" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension: ease_of_discovery {
    type: string
    sql: ${TABLE}."ease_of_discovery" ;;
  }

  dimension: ease_of_exploit {
    type: string
    sql: ${TABLE}."ease_of_exploit" ;;
  }

  dimension: impact {
    type: string
    sql: ${TABLE}."impact" ;;
  }

  dimension: integrity_impact {
    type: string
    sql: ${TABLE}."integrity_impact" ;;
  }

  dimension_group: internal_report {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."internal_report_date" ;;
  }

  dimension: intrustion_detection {
    type: string
    sql: ${TABLE}."intrustion_detection" ;;
  }

  dimension: owasp {
    type: string
    sql: ${TABLE}."owasp" ;;
  }

  dimension_group: reopened {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."reopened_date" ;;
  }

  dimension: severity_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension_group: submitted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."submitted_date" ;;
  }

  dimension: subsidiary {
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}."tags" ;;
  }

  dimension: technical_department {
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: tps {
    type: string
    sql: ${TABLE}."tps" ;;
  }

  dimension: vendor_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: vulnerability_name {
    type: string
    sql: ${TABLE}."vulnerability_name" ;;
  }

  dimension_group: vulnerability_national_report {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."vulnerability_national_report_date" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      detected_vulnerability_id,
      vulnerability_name,
      vendors.vendor_id,
      vendors.vendor_name,
      status.status_id,
      severity.severity_id
    ]
  }
}
