view: process_vendors {
  sql_table_name: abb.process_vendors ;;

  dimension: process_id {
    type: string
    sql: ${TABLE}."process_id" ;;
  }

  dimension: vendor_id {
    type: string
    sql: ${TABLE}."vendor_id" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
