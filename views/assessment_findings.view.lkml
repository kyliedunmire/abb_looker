view: assessment_findings {
  sql_table_name: abb.assessment_findings ;;

  dimension: assessment_findings_id {
    hidden: yes
    primary_key: yes
    type: string
    sql: ${TABLE}."assessment_findings_id" ;;
  }

  dimension: assessment_id {
    type: string
    hidden: yes
    sql: ${TABLE}."assessment_id" ;;
  }

  dimension: assigned_to_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: assigned_to {
    group_label: "Assigned To"
    group_item_label: "Name"
    type: string
    sql: ${finding_assigned.full_name} ;;
  }

  dimension: assigned_to_email {
    group_label: "Assigned To"
    group_item_label: "Email"
    type: string
    sql: ${finding_assigned.email} ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension_group: closed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."closed_date" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: cybersecurity_framework_category {
    type: string
    sql: ${TABLE}."cybersecurity_framework_category" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."finding_description" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."finding_name" ;;
    html: <a href="https://fis-mtn.fortressis.com/assessmentFindings/details/{{ assessment_findings.assessment_findings_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}."finding_notes" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."finding_type" ;;
  }

  dimension: impact {
    type: number
    sql: ${TABLE}."impact"::int ;;
  }

  dimension: iso_mapping {
    type: string
    sql: ${TABLE}."iso_mapping" ;;
  }

  dimension: likelihood {
    type: number
    sql: ${TABLE}."likelihood"::int ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: recommendation {
    type: string
    sql: ${TABLE}."recommendation" ;;
  }

  dimension: resolution_id {
    hidden: yes
    type: string
    sql: ${TABLE}."resolution" ;;
  }

  dimension: severity_id {
    type: string
    hidden: yes
    sql: ${TABLE}."finding_severity_id" ;;
  }

  dimension: speed_and_onset {
    type: number
    sql: ${TABLE}."speed_and_onset"::int ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: vendor_id {
    type: string
    hidden: yes
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: vulnerability {
    type: string
    sql: ${TABLE}."vulnerability" ;;
  }

  dimension: coming_due_past_due_general {
    label: "Coming Due/Past Due (General)"
    group_label: "Coming Due/Past Due"
    group_item_label: "General"
    type: string
    sql: CASE
         WHEN ${status_id} IN ('LphMEuKJ9hPyfkLuW','MXgkXQBeaRjFhuYbc')
         THEN 'Closed'
         WHEN extract(days from (${due_date} - now())) < 0
         THEN 'Past due'
         WHEN extract(days from (${due_date} - now())) < 7
         THEN 'Coming due'
         ELSE 'No escalation required'
         END;; #There are 2 assessment finding status documents, bringing in the back end ID of both just in case
    description: "Generalizing all incomplete Findings as either past due or coming due within 2 days"
  }

  dimension: coming_due_past_due_detailed {
    label: "Coming Due/Past Due (Detailed)"
    group_label: "Coming Due/Past Due"
    group_item_label: "Detailed"
    type: string
    sql: CASE
          WHEN ${status_id} IN ('LphMEuKJ9hPyfkLuW','MXgkXQBeaRjFhuYbc')
          THEN 'Closed'
          WHEN extract(days from (${due_date} - now())) < -6
          THEN '>7 days past due'
          WHEN extract(days from (${due_date} - now())) < 0
          THEN '<7 days past due'
          WHEN extract(days from (${due_date} - now())) < 7
          THEN '<7 days coming due'
          ELSE '>7 days coming due'
          END;; #There are 2 assessment finding status documents, bringing in the back end ID of both just in case
    description: "Providing details about the state of coming due/past due - More than a week past due, within a week past due, coming due within a week, coming due within more than a week."
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}.class_id ;;
  }

  dimension: iso_domain {
    type: string
    sql: ${TABLE}.iso_domain ;;
  }

  dimension: iso_control_description {
    type: string
    sql: ${TABLE}.iso_control_description ;;
  }

##----------------------------------------------------------------------------------------------------------     Measures

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      platform_id,
      name,
      finding_severity.severity,
      finding_status.status,
      assigned_to,
      assessments.name,
      cybersecurity_framework_category
    ]
  }
}
