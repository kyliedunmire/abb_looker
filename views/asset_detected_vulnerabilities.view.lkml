view: asset_detected_vulnerabilities {
  sql_table_name: abb.asset_detected_vulnerabilities ;;

  dimension: asset_id {
    type: string
    hidden: yes
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: detected_vulnerability_id {
    type: string
    sql: ${TABLE}."detected_vulnerability_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [assets.asset_name, assets.asset_id]
  }
}
