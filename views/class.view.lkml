view: class {
  sql_table_name: abb.class ;;
  drill_fields: [class_id]

  dimension: class_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."class_id" ;;
  }

  dimension: class {
    type: string
    sql: ${TABLE}."class_label" ;;
  }

  dimension: class_type {
    type: string
    sql: ${TABLE}.class_type ;;
  }

  measure: count {
    type: count
    drill_fields: [class_id]
  }
}
