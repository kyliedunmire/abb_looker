view: workflow {
  sql_table_name: abb.workflow ;;
  drill_fields: [workflow_pk]

  dimension: workflow_pk {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}."workflow_pk" ;;
  }

  dimension: assignee_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assignee" ;;
  }

  dimension_group: _ {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."date" ;;
  }

  dimension: days_to_complete {
    type: number
    sql: ${TABLE}."days_to_complete" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension_group: est_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."est_completed_date" ;;
  }

  dimension: object_id {
    hidden: yes
    type: string
    sql: ${TABLE}."object_id" ;;
  }

  dimension: phase {
    type: string
    sql: ${TABLE}."phase" ;;
  }

  dimension: phase_sort {
    type: number
    sql: ${TABLE}."phase_sort" ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: step {
    type: string
    sql: ${TABLE}."step" ;;
  }

  dimension: step_assigned_to {
    type: string
    sql: ${TABLE}."step_assigned_to" ;;
  }

  dimension_group: step_due {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."step_due_date" ;;
  }

  dimension: step_performance {
    type: number
    sql: ${TABLE}."step_performance" ;;
  }

  dimension: step_sort {
    type: number
    sql: ${TABLE}."step_sort" ;;
  }

  dimension: step_user_id {
    hidden: yes
    type: string
    sql: ${TABLE}."user_id" ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}."tags" ;;
  }

  dimension: task {
    type: string
    sql: ${TABLE}."task" ;;
  }

  dimension_group: task_due {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."task_due_date" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension: user_id {
    type: string
    hidden: yes
    sql: ${TABLE}."user_id" ;;
  }

  dimension: workflow_step {
    type: string
    sql: ${TABLE}."workflow_step" ;;
  }

  measure: count {
    type: count
    drill_fields: [workflow_pk, users.first_name, users.user_id, users.last_name, status.status_id]
  }
}
