view: survey_outcomes {
  sql_table_name: abb.survey_outcomes ;;
  drill_fields: [survey_outcome_id]

  dimension: survey_outcome_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."survey_outcome_id" ;;
  }

  dimension: answer_id {
    type: string
    sql: ${TABLE}."answer_id" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: freeform_answer {
    type: string
    sql: ${TABLE}."freeform_answer" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: question_id {
    type: string
    sql: ${TABLE}."question_id" ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}."status" ;;
  }

  dimension: survey_instance_id {
    type: string
    sql: ${TABLE}."survey_instance_id" ;;
  }

  dimension: survey_template_id {
    type: string
    sql: ${TABLE}."survey_template_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [survey_outcome_id]
  }
}
