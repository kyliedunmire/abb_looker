view: assets {
  sql_table_name: abb.assets ;;
  drill_fields: [asset_id]

  dimension: asset_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: accountability_loss {
    group_label: "Technical Impact"
    type: number
    sql: ${TABLE}."accountability_loss" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."asset_name" ;;
  }

  dimension: assigned_to_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: assigned_to {
    group_label: "Assigned To"
    group_item_label: "Name"
    type: string
    sql: ${asset_assigned.full_name} ;;
  }

  dimension: assigned_to_email {
    group_label: "Assigned To"
    group_item_label: "Email"
    type: string
    sql: ${asset_assigned.email} ;;
  }

  dimension: availability_loss {
    group_label: "Technical Impact"
    type: number
    sql: ${TABLE}."availability_loss" ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension: confidentiality_loss {
    group_label: "Technical Impact"
    type: number
    sql: ${TABLE}."confidentiality_loss" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: created_by {
    group_label: "Created By"
    group_item_label: "Name"
    type: string
    sql: ${asset_created.full_name} ;;
  }

  dimension: created_by_email {
    group_label: "Created By"
    group_item_label: "Email"
    type: string
    sql: ${asset_created.email} ;;
  }

  dimension: financial_damage {
    group_label: "Business Impact"
    type: number
    sql: ${TABLE}."financial_damage_impact" ;;
  }

  dimension: impact {
    type: string
    sql: ${TABLE}."impact" ;;
  }

  dimension: integrity_loss {
    group_label: "Technical Impact"
    type: number
    sql: ${TABLE}."integrity_loss" ;;
  }

  dimension: ip_addresses {
    type: string
    sql: ${TABLE}."ip_addresses" ;;
  }

  dimension: non_compliance {
    group_label: "Business Impact"
    type: number
    sql: ${TABLE}."non_compliance_impact" ;;
  }

  dimension: os_family {
    group_label: "OS"
    group_item_label: "Family"
    type: string
    sql: ${TABLE}."os_family" ;;
  }

  dimension: os_vendor {
    group_label: "OS"
    group_item_label: "Vendor"
    type: string
    sql: ${TABLE}."os_vendor" ;;
  }

  dimension: os_version {
    group_label: "OS"
    group_item_label: "Version"
    type: string
    sql: ${TABLE}."os_version" ;;
  }

  dimension: owasp {
    label: "OWASP"
    type: string
    sql: ${TABLE}."owasp" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: privacy_violation {
    group_label: "Business Impact"
    type: number
    sql: ${TABLE}."privacy_violation_impact" ;;
  }

  dimension: reputational_damage {
    group_label: "Business Impact"
    type: number
    sql: ${TABLE}."reputational_damage_impact" ;;
  }

  dimension: safety {
    group_label: "Business Impact"
    type: number
    sql: ${TABLE}."safety_impact" ;;
  }

  dimension: severity_id {
    type: string
    hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: tps {
    label: "TPS"
    type: string
    sql: ${TABLE}."tps" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: updated_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."updated_by" ;;
  }

  dimension: updated_by {
    group_label: "Updated By"
    group_item_label: "Name"
    type: string
    sql: ${asset_updated.full_name} ;;
  }

  dimension: updated_by_email {
    group_label: "Updated By"
    group_item_label: "Email"
    type: string
    sql: ${asset_updated.email} ;;
  }

  dimension: process_id {
    hidden: yes
    type: string
    sql: ${TABLE}.process_id ;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}.class_id;;
  }

  #############---------------------------------------------------------------------------------------

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      platform_id,
      name
    ]
  }
}
