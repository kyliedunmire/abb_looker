connection: "mtn_ddrr"

datagroup: abb_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: abb_default_datagroup

include: "*.view"
include: "*.dashboard"

explore: procurements {
  from: processes

  join: procurement_criticality{
    from: severity
    fields: [procurement_criticality.criticality]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.severity_id} = ${procurement_criticality.severity_id} AND ${procurement_criticality.severity_type} = 'processes-severity';;
  }

  join: procurement_workflow_complete {
    from: workflow_complete
    view_label: "Procurements"
    fields: []
    relationship: many_to_one
    sql_on: ${procurements.process_id} = ${procurement_workflow_complete.object_id} ;;
  }

  join: procurement_business_department {
    from: business_department
    view_label: "Procurements"
    fields: [procurement_business_department.business_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.business_department} = ${procurement_business_department.business_department_id} ;;
  }

  join: procurements_technical_department {
    from: technical_department
    view_label: "Procurements"
    fields: [procurements_technical_department.technical_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.technical_department} = ${procurements_technical_department.technical_department_id} ;;
  }

  join: procurements_subsidiary {
    from: subsidiary
    view_label: "Procurements"
    fields: [procurements_subsidiary.subsidiary]
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.subsidiary} = ${procurements_subsidiary.subsidiary_id} ;;
  }

  join: process_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.created_by_id} = ${process_created.user_id} ;;
  }
  join: procurement_status {
    from: status
    fields: [procurement_status.status]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.status_id} = ${procurement_status.status_id} AND ${procurement_status.status_type} = 'processes-status';;
  }

  join: procurement_severity {
    from: severity
    fields: [procurement_severity.severity]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.severity_id} = ${procurement_severity.severity_id} AND ${procurement_severity.severity_type} = 'processes-severity';;
  }

  join: procurement_class {
    from: class
    fields: [procurement_class.class]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.class_id} = ${procurement_class.class_id} AND ${procurement_class.class_type} = 'processes-class';;
  }

  join: procurement_resolution {
    from: resolutions
    fields: [procurement_resolution.resolution]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.resolution_id} = ${procurement_resolution.resolution_id} AND ${procurement_resolution.resolution_type} = 'processes-resolution';;
  }

  join: procurement_qa {
    view_label: "Procurements"
    type: left_outer
    relationship: one_to_one
    sql_on: ${procurements.process_id} = ${procurement_qa.survey_outcome_id} ;;
  }

  join: procurement_workflow {
    from: workflow
    type: left_outer
    relationship: one_to_many
    sql_on: ${procurements.process_id} = ${procurement_workflow.object_id} ;;
  }

  join: processes_workflow_sla {
    view_label: "Procurement SLA"
    type: left_outer
    relationship: one_to_many
    sql_on: ${procurements.process_id} = ${processes_workflow_sla.object_id} ;;
  }

  join: assignee {
    from: users
    fields: []
    relationship: many_to_one
    sql_on: ${processes_workflow_sla.assigned_id} = ${assignee.user_id} ;;
  }


  ##-----------------------------------------------------------------------------------    Vendors


  join: process_vendors {
    fields: []
    type: left_outer
    relationship: one_to_many
    sql_on: ${procurements.process_id} = ${process_vendors.process_id} ;;
  }

  join: vendors {
    type: left_outer
    relationship: many_to_one
    sql_on: ${process_vendors.vendor_id} = ${vendors.vendor_id} ;;
  }

  join: vendor_criticality{
    from: severity
    fields: [vendor_criticality.criticality]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.severity_id} = ${vendor_criticality.severity_id} AND ${vendor_criticality.severity_type} = 'vendors-severity';;
  }

  join: vendor_class {
    from: class
    fields: [vendor_class.class]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.class_id} = ${vendor_class.class_id} AND ${vendor_class.class_type} = 'vendors-class' ;;
  }



#   ###############################################################      Procurement Path Questionnaire
#
#   join: procurement_path_questionnaire {
#     type: left_outer
#     relationship: one_to_one
#     sql_on: ${procurements.process_id} = ${procurement_path_questionnaire.process_id} ;;
#   }
#
#

  ###############################################################      Risk Ranks

  join: risk_ranks {
    from: services
    type: left_outer
    relationship: one_to_many
    sql_on: ${procurements.process_id} = ${risk_ranks.process_id} ;;
  }

  join: service_assigned {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${risk_ranks.assigned_to_id} = ${service_assigned.user_id} ;;
  }

  join: service_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${risk_ranks.created_by_id} = ${service_created.user_id} ;;
  }

  join: service_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${risk_ranks.submitted_by_id} = ${service_submitted.user_id} ;;
  }

  join: service_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${risk_ranks.updated_by_id} = ${service_updated.user_id} ;;
  }

  join: service_status {
    from: status
    fields: [service_status.status]
    view_label: "Risk Ranks"
    type: left_outer
    relationship: many_to_one
    sql_on: ${risk_ranks.status_id} = ${service_status.status_id} ;;
  }

  join: service_severity {
    from: severity
    fields: [service_severity.severity]
    view_label: "Risk Ranks"
    type: left_outer
    relationship: many_to_one
    sql_on: ${risk_ranks.severity_id} = ${service_severity.severity_id} ;;
  }



  ##------------------------------------------------------------------------------    Applications


  join: applications {
    type: left_outer
    relationship: one_to_many
    sql_on: ${procurements.process_id} = ${applications.process_id} ;;
  }

  join: application_workflow_complete {
    from: workflow_complete
    view_label: "Applications"
    fields: []
    relationship: many_to_one
    sql_on: ${applications.application_id} = ${application_workflow_complete.object_id} ;;
  }

  join: application_class {
    from: class
    fields: [application_class.class]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.class_id} = ${application_class.class_id} ;;
  }

  join: application_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.created_by_id} = ${application_created.user_id} ;;
  }

  join: application_resolution {
    from: resolutions
    fields: [application_resolution.resolution]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.resolution_id} = ${application_resolution.resolution_id} ;;
  }

  join: application_severity {
    from: severity
    fields: [application_severity.severity]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${application_severity.severity_id} ;;
  }

  join: application_status {
    from: status
    fields: [application_status.status]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.status_id} = ${application_status.status_id} ;;
  }

  join: application_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.updated_by_id} = ${application_updated.user_id} ;;
  }


  ##---------------------------------------------------------------------------------------     Assessments

  join: process_assessments {
    fields: []
    type: left_outer
    relationship: one_to_many
    sql_on: ${procurements.process_id} = ${process_assessments.process_id} ;;
  }

  join: assessments {
    type: left_outer
    relationship: many_to_one
    sql_on: ${process_assessments.assessment_id} = ${assessments.assessment_id} ;;
  }

  join: assessment_workflow_complete {
    from: workflow_complete
    view_label: "Assessments"
    fields: []
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_complete.object_id} ;;
  }

  join: assessment_assigned {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assigned_to_id} = ${assessment_assigned.user_id} ;;
  }

  join: assessment_class {
    from: class
    fields: [assessment_class.class]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.class_id} = ${assessment_class.class_id} AND ${assessment_class.class_type} = 'assessments-class';;
  }

  join: assessment_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.created_by_id} = ${assessment_created.user_id} ;;
  }

  join: assessments_resolution {
    from: resolutions
    fields: [assessments_resolution.resolution]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.resolution_id} = ${assessments_resolution.resolution_id} AND ${assessments_resolution.resolution_type} = 'assessments-resolution';;
  }

  join: assessment_status {
    from: status
    fields: [assessment_status.status]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.status_id} = ${assessment_status.status_id} AND ${assessment_status.status_type} = 'assessments-status';;
  }

  join: assessment_severity {
    from: severity
    fields: [assessment_severity.severity]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_severity.severity_id} AND ${assessment_severity.severity_type} = 'assessments-severity';;
  }

  join: assessment_criticality{
    from: severity
    fields: [assessment_criticality.criticality]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_criticality.severity_id} AND ${assessment_criticality.severity_type} = 'assessments-severity';;
  }



  join: assessment_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.submitted_by_id} = ${assessment_submitted.user_id} ;;
  }

  join: assessment_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.updated_by_id} = ${assessment_updated.user_id} ;;
  }



  ##---------------------------------------------------------         Findings

  join: assessment_findings {
    type: left_outer
    relationship: one_to_many
    sql_on: ${assessments.assessment_id} = ${assessment_findings.assessment_id} ;;
  }

  join: finding_status {
    from: status
    fields: [finding_status.status]
    view_label: "Assessment Findings"
    relationship: many_to_one
    sql_on: ${assessment_findings.status_id} = ${finding_status.status_id} AND ${finding_status.status_type} = 'assessmentFindings-status';;
  }

  join: finding_severity {
    from: severity
    fields: [finding_severity.severity]
    view_label: "Assessment Findings"
    relationship: many_to_one
    sql_on: ${assessment_findings.severity_id} = ${finding_severity.severity_id} AND ${finding_severity.severity_type} = 'assessmentFindings-severity';;
  }

  join: finding_assigned {
    from: users
    fields: []
    relationship: many_to_one
    sql_on: ${assessment_findings.assigned_to_id} = ${finding_assigned.user_id} ;;
  }

  join: finding_class {
    from: class
    fields: [finding_class.class]
    view_label: "Assessment Findings"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.class_id} = ${finding_class.class_id} ;;
  }



  ##----------------------------------------------------------------------      Assets

  join: assets {
    type: left_outer
    relationship: one_to_many
    sql_on: ${procurements.process_id} = ${assets.process_id} ;;
  }

  join: asset_assigned {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.assigned_to_id} = ${asset_assigned.user_id} ;;
  }

  join: asset_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.created_by_id} = ${asset_created.user_id} ;;
  }

  join: asset_severity {
    from: severity
    fields: [asset_severity.severity]
    view_label: "Assets"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.severity_id} = ${asset_severity.severity_id} ;;
  }

  join: asset_status {
    from: status
    fields: [asset_status.status]
    view_label: "Assets"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.status_id} = ${asset_status.status_id} ;;
  }

  join: asset_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.updated_by_id} = ${asset_updated.user_id} ;;
  }

  join: asset_class {
    from: class
    fields: [asset_class.class]
    view_label: "Assets"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.class_id} = ${asset_class.class_id} ;;
  }

}


#################################################################################################################
#################################################################################################################
#################################################################################################################


explore: vendors {

  join: vendor_criticality{
    from: severity
    fields: [vendor_criticality.criticality]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.severity_id} = ${vendor_criticality.severity_id} AND ${vendor_criticality.severity_type} = 'vendors-severity';;
  }

  join: engagement_scopes {
    from: engagement_scopes
    view_label: "Engagement Scopes"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.vendor_id} = ${engagement_scopes.vendor_id} ;;
  }

  join: status {
    view_label: "Engagement Scopes"
    type: left_outer
    relationship: many_to_one
    sql_on: ${engagement_scopes.status_id} = ${status.status_id} ;;
  }

  join: vendor_business_department {
    from: business_department
    view_label: "Vendors"
    fields: [vendor_business_department.business_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.business_department} = ${vendor_business_department.business_department_id} ;;
  }

  join: vendor_technical_department {
    from: technical_department
    view_label: "Vendors"
    fields: [vendor_technical_department.technical_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.technical_department} = ${vendor_technical_department.technical_department_id} ;;
  }

  join: vendors_subsidiary {
    from: subsidiary
    view_label: "Vendors"
    fields: [vendors_subsidiary.subsidiary]
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.subsidiary} = ${vendors_subsidiary.subsidiary_id} ;;
  }

  join: vendor_class {
    from: class
    fields: [vendor_class.class]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.class_id} = ${vendor_class.class_id} ;;
  }

  join: assessments {
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.vendor_id} = ${assessments.vendor_id} ;;
  }

  join: assessment_workflow_complete {
    from: workflow_complete
    view_label: "Assessments"
    fields: []
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_complete.object_id} ;;
  }

  join: assessment_criticality{
    from: severity
    fields: [assessment_criticality.criticality]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_criticality.severity_id} AND ${assessment_criticality.severity_type} = 'assessments-severity';;
  }

  join: assessment_status {
    from: status
    view_label: "Assessments"
    fields: [assessment_status.status]
    relationship: many_to_one
    sql_on: ${assessments.status_id} = ${assessment_status.status_id} ;;
  }

  join: assessment_assigned {
    view_label: "Assessments"
    from: users
    fields: [assessment_assigned.full_name]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assigned_to} = ${assessment_assigned.user_id} ;;
  }

  join: assessment_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.created_by_id} = ${assessment_created.user_id} ;;
  }

  join: assessment_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.submitted_by_id} = ${assessment_submitted.user_id} ;;
  }

  join: assessment_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.updated_by_id} = ${assessment_updated.user_id} ;;
  }

  join: assessment_class {
    from: class
    fields: [assessment_class.class]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.class_id} = ${assessment_class.class_id} ;;
  }

  join: applications {
    view_label: "Products"
    type: left_outer
    relationship: one_to_many
    sql_on: ${vendors.vendor_id} = ${applications.vendor_id} ;;
  }

  join: applications_criticality{
    from: severity
    fields: [applications_criticality.criticality]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${applications_criticality.severity_id} AND ${applications_criticality.severity_type} = 'applications-severity';;
  }

  join: application_workflow_complete {
    from: workflow_complete
    view_label: "Products"
    fields: []
    relationship: many_to_one
    sql_on: ${applications.application_id} = ${application_workflow_complete.object_id} ;;
  }


  join: application_class {
    from: class
    fields: [application_class.class]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.class_id} = ${application_class.class_id} ;;
  }

  join: application_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.created_by_id} = ${application_created.user_id} ;;
  }

  join: application_resolution {
    from: resolutions
    fields: [application_resolution.resolution]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.resolution_id} = ${application_resolution.resolution_id} ;;
  }

  join: application_severity {
    from: severity
    fields: [application_severity.severity]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${application_severity.severity_id} ;;
  }

  join: application_status {
    from: status
    fields: [application_status.status]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.status_id} = ${application_status.status_id} ;;
  }

  join: application_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.updated_by_id} = ${application_updated.user_id} ;;
  }
}



###################################################################################################################################################################
###################################################################################################################################################################



explore: solution {
  from: solution_patchchain

  ####------------------------------------------------------     Application

  join: applications {
    type: left_outer
    relationship: one_to_many
    sql_on: ${solution.application_id} = ${applications.application_id} ;;
  }

  join: application_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.created_by_id} = ${application_created.user_id} ;;
  }

  join: application_workflow_complete {
    from: workflow_complete
    view_label: "Applications"
    fields: []
    relationship: many_to_one
    sql_on: ${applications.application_id} = ${application_workflow_complete.object_id} ;;
  }

  join: application_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.updated_by_id} = ${application_updated.user_id} ;;
  }

  join: application_class {
    from: class
    fields: [application_class.class]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.class_id} ${application_class.class_id} ;;
  }

  join: application_resolution {
    from: resolutions
    fields: [application_resolution.resolution]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.resolution_id} = ${application_resolution.resolution_id} ;;
  }

  join: application_severity {
    from: severity
    fields: [application_severity.severity]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${application_severity.severity_id} ;;
  }

  join: application_status {
    from: status
    fields: [application_status.status]
    view_label: "Applications"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.status_id} = ${application_status.status_id} ;;
  }

  ##-------------------------------------------------------------------------------     Assets

  join: assets {
    type: left_outer
    relationship: one_to_many
    sql_on: ${solution.asset_id} = ${assets.asset_id} ;;
  }

  join: asset_assigned {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.assigned_to_id} = ${asset_assigned.user_id} ;;
  }

  join: asset_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.created_by_id} = ${asset_created.user_id} ;;
  }

  join: asset_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.updated_by_id} = ${asset_updated.user_id} ;;
  }

  join: asset_severity {
    from: severity
    fields: [asset_severity.severity]
    view_label: "Assets"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.severity_id} = ${asset_severity.severity_id} ;;
  }

  join: asset_status {
    from: status
    fields: [asset_status.status]
    view_label: "Assets"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.status_id} = ${asset_status.status_id} ;;
  }

  join: asset_class {
    from: class
    fields: [asset_class.class]
    view_label: "Assets"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assets.class_id} ${asset_class.class_id} ;;
  }


## ------------------------------------------------------------------------ Assessessments
  join: assessments {
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.application_id} = ${applications.application_id} ;;
  }

  join: assessment_workflow_complete {
    from: workflow_complete
    view_label: "Assessments"
    fields: []
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_complete.object_id} ;;
  }

  join: assessment_assigned {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assigned_to_id} = ${assessment_assigned.user_id} ;;
  }

  join: assessment_class {
    from: class
    fields: [assessment_class.class]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.class_id} = ${assessment_class.class_id} AND ${assessment_class.class_type} = 'assessments-class';;
  }

  join: assessment_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.created_by_id} = ${assessment_created.user_id} ;;
  }

  join: assessments_resolution {
    from: resolutions
    fields: [assessments_resolution.resolution]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.resolution_id} = ${assessments_resolution.resolution_id} AND ${assessments_resolution.resolution_type} = 'assessments-resolution';;
  }

  join: assessment_status {
    from: status
    fields: [assessment_status.status]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.status_id} = ${assessment_status.status_id} AND ${assessment_status.status_type} = 'assessments-status';;
  }

  join: assessment_severity {
    from: severity
    fields: [assessment_severity.severity]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_severity.severity_id} AND ${assessment_severity.severity_type} = 'assessments-severity';;
  }

  join: assessment_criticality{
    from: severity
    fields: [assessment_criticality.criticality]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_criticality.severity_id} AND ${assessment_criticality.severity_type} = 'assessments-severity';;
  }

  join: assessment_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.submitted_by_id} = ${assessment_submitted.user_id} ;;
  }

  join: assessment_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.updated_by_id} = ${assessment_updated.user_id} ;;
  }
}

###########################################################################     Findings

explore: assessment_findings {

  join: finding_status {
    from: status
    fields: [finding_status.status]
    view_label: "Assessment Findings"
    type: left_outer
    relationship: one_to_many
    sql_on: ${assessment_findings.status_id} = ${finding_status.status_id}  ;;
  }

  join: findings_class {
    from: class
    fields: [findings_class.class]
    view_label: "Assessment Findings"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.class_id} = ${findings_class.class_id} ;;
  }

  join: assessments {
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.assessment_id} = ${assessments.assessment_id} ;;
  }
  join: assessment_workflow_complete {
    from: workflow_complete
    view_label: "Assessments"
    fields: []
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_complete.object_id} ;;
  }

  join: assessments_resolution {
    from:  resolutions
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.resolution_id} = ${assessments_resolution.resolution_id} ;;
  }

  join: assessment_assigned {
    view_label: "Assessments"
    from: users
    fields: [assessment_assigned.full_name]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assigned_to} = ${assessment_assigned.user_id} ;;
  }

  join: assessment_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.created_by_id} = ${assessment_created.user_id} ;;
  }

  join: assessment_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.submitted_by_id} = ${assessment_submitted.user_id} ;;
  }

  join: assessment_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.updated_by_id} = ${assessment_updated.user_id} ;;
  }

  join: assessment_class {
    from: class
    fields: [assessment_class.class]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.class_id} = ${assessment_class.class_id} ;;
  }

  join: assessment_status {
    from: status
    view_label: "Assessments"
    fields: [assessment_status.status]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.status_id} = ${assessment_status.status_id} ;;
  }

  join: resolutions {
    view_label: "Assessment Findings"
    fields: [resolutions.resolution]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.resolution_id} = ${resolutions.resolution_id} ;;
  }

  join: finding_severity {
    from: severity
    view_label: "Assessment Findings"
    fields: [finding_severity.severity]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.severity_id} = ${finding_severity.severity_id} ;;
  }

  join: assessment_findings_workflow_sla {
    view_label: "Workflow"
    type: left_outer
    relationship: one_to_one
    sql_on: ${assessment_findings.assessment_findings_id} = ${assessment_findings_workflow_sla.object_id} ;;
  }

  join: assignee {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings_workflow_sla.assigned_id} = ${assignee.user_id} ;;
  }

  join: assessment_severity {
    from: severity
    view_label: "Assessments"
    fields: [assessment_severity.severity]
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_severity.severity_id} ;;
  }

  join: assessment_criticality{
    from: severity
    fields: [assessment_criticality.criticality]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_criticality.severity_id} AND ${assessment_criticality.severity_type} = 'assessments-severity';;
  }

  join: assessment_findings_current_workflow {
    view_label: "Current Workflow"
    type: left_outer
    relationship: one_to_one
    sql_on: ${assessment_findings.assessment_findings_id} = ${assessment_findings_current_workflow.object_id} ;;
  }

  join: current_workflow_status {
    from: status
    fields: [current_workflow_status.status]
    view_label: "Current Workflow"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings_current_workflow.status_id} = ${current_workflow_status.status_id} ;;
  }

  join: current_workflow_assignee {
    view_label: "Current Workflow"
    from: users
    fields: [current_workflow_assignee.full_name]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings_current_workflow.assignee_id} = ${current_workflow_assignee.user_id} ;;
  }

  join: business_department {
    view_label: "Assessment Findings"
    fields: [business_department.business_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.business_department} = ${business_department.business_department_id} ;;
  }

  join: technical_department {
    view_label: "Assessment Findings"
    fields: [technical_department.technical_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.technical_department} = ${technical_department.technical_department_id} ;;
  }

  join: subsidiary {
    view_label: "Assessment Findings"
    fields: [subsidiary.subsidiary]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.subsidiary} = ${subsidiary.subsidiary_id} ;;
  }

  join: vendors {
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.vendor_id} = ${vendors.vendor_id} ;;
  }

  join: vendor_severity {
    from: severity
    view_label: "Vendors"
    fields: [vendor_severity.severity]
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.severity_id} = ${vendor_severity.severity_id} ;;
  }

  join: vendors_class {
    from: class
    fields: [vendors_class.class]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.class_id} = ${vendors_class.class_id} ;;
  }

  join: applications {
    type: left_outer
    relationship: one_to_many
    sql_on: ${vendors.vendor_id} = ${applications.vendor_id} ;;
  }

  join: applications_criticality{
    from: severity
    fields: [applications_criticality.criticality]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${applications_criticality.severity_id} AND ${applications_criticality.severity_type} = 'applications-severity';;
  }

  join: application_workflow_complete {
    from: workflow_complete
    view_label: "Products"
    fields: []
    relationship: many_to_one
    sql_on: ${applications.application_id} = ${application_workflow_complete.object_id} ;;
  }

  join: application_class {
    from: class
    fields: [application_class.class]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.class_id} = ${application_class.class_id} ;;
  }

  join: application_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.created_by_id} = ${application_created.user_id} ;;
  }

  join: application_resolution {
    from: resolutions
    fields: [application_resolution.resolution]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.resolution_id} = ${application_resolution.resolution_id} ;;
  }

  join: application_severity {
    from: severity
    fields: [application_severity.severity]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${application_severity.severity_id} ;;
  }

  join: application_status {
    from: status
    fields: [application_status.status]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.status_id} = ${application_status.status_id} ;;
  }

  join: application_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.updated_by_id} = ${application_updated.user_id} ;;
  }

  join: assessment_workflow_sla_test {
    from: workflow_sla_totals
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_sla_test.object_id} ;;
  }

  join: findings_workflow_sla_totals {
    from: workflow_sla_totals
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.assessment_findings_id} = ${findings_workflow_sla_totals.object_id} ;;
  }

  join: finding_assigned {
    from: users
    fields: []
    relationship: many_to_one
    sql_on: ${assessment_findings.assigned_to_id} = ${finding_assigned.user_id} ;;
  }
}




###############################################################################################################################################################     All Objects

explore: all_objects {
  from: _vendors_comingdue_pastdue
  view_label: "Vendors"

  join: services {
    view_label: "Risk Ranking"
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.vendor_id} = ${services.vendor_id} ;;
  }

  join: engagement_scopes {
    from: engagement_scopes
    view_label: "Engagement Scopes"
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.vendor_id} = ${engagement_scopes.vendor_id} ;;
  }

  join: status {
    view_label: "Engagement Scopes"
    type: left_outer
    relationship: many_to_one
    sql_on: ${engagement_scopes.status_id} = ${status.status_id} ;;
  }

  join: vendor_tiering_status {
    from: status
    view_label: "Risk Ranking"
    fields: [vendor_tiering_status.status]
    relationship: many_to_one
    sql_on: ${services.status_id} = ${vendor_tiering_status.status_id} ;;
  }

  join: vendor_class {
    from: class
    fields: [vendor_class.class]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.class_id}=${vendor_class.class_id} ;;
  }

  join: assessments {
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.vendor_id} = ${assessments.vendor_id} ;;
  }
  join: assessment_workflow_complete {
    from: workflow_complete
    view_label: "Assessments"
    fields: []
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_complete.object_id} ;;
  }

  join: assessment_status {
    from: status
    view_label: "Assessments"
    fields: [assessment_status.status]
    relationship: many_to_one
    sql_on: ${assessments.status_id} = ${assessment_status.status_id} ;;
  }

  join: assessment_findings {
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.vendor_id} = ${assessment_findings.vendor_id} ;;
  }

  join: findings_class {
    from: class
    fields: [findings_class.class]
    view_label: "Assessment Findings"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.class_id} = ${findings_class.class_id} ;;
  }

  join: assessments_resolution {
    from:  resolutions
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.resolution_id} = ${assessments_resolution.resolution_id} ;;
  }

  join: assessment_assigned {
    view_label: "Assessments"
    from: users
    fields: [assessment_assigned.full_name]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assigned_to} = ${assessment_assigned.user_id} ;;
  }

  join: assessment_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.created_by_id} = ${assessment_created.user_id} ;;
  }

  join: assessment_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.submitted_by_id} = ${assessment_submitted.user_id} ;;
  }

  join: assessment_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.updated_by_id} = ${assessment_updated.user_id} ;;
  }

  join: assessment_class {
    from: class
    fields: [assessment_class.class]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.class_id} = ${assessment_class.class_id} ;;
  }

  join: vendor_severity {
    from: severity
    view_label: "Vendors"
    fields: [vendor_severity.severity]
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.severity_id} = ${vendor_severity.severity_id} ;;
  }

  join: finding_severity {
    from: severity
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.severity_id} = ${finding_severity.severity_id} ;;
  }

  join: assessment_severity {
    from: severity
    view_label: "Assessments"
    fields: [assessment_severity.severity]
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_severity.severity_id} ;;
  }

  join: assessment_criticality{
    from: severity
    fields: [assessment_criticality.criticality]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_criticality.severity_id} AND ${assessment_criticality.severity_type} = 'assessments-severity';;
  }

  join: finding_status {
    from: status
    view_label: "Assessment Findings"
    fields: [finding_status.status]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.status_id} = ${finding_status.status_id} ;;
  }

  join: vendors_closed_findings {
    view_label: "Vendors"
    fields: [vendors_closed_findings.no_closed_findings, vendors_closed_findings.uncooperative_vendor]
    type: left_outer
    relationship: one_to_one
    sql_on: ${all_objects.vendor_id} = ${vendors_closed_findings.vendor_id} ;;
  }

  join: business_department {
    view_label: "Vendors"
    fields: [business_department.business_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.business_department} = ${business_department.business_department_id} ;;
  }

  join: technical_department {
    view_label: "Vendors"
    fields: [technical_department.technical_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.technical_department} = ${technical_department.technical_department_id} ;;
  }

  join: subsidiary {
    view_label: "Vendors"
    fields: [subsidiary.subsidiary]
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.subsidiary} = ${subsidiary.subsidiary_id} ;;
  }

  join: assessment_workflow_sla {
    from: workflow_sla_totals
    fields: [assessment_workflow_sla.average_days_to_complete_total_workflow]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_sla.object_id} ;;
  }


  join: findings_workflow_sla_totals {
    from: workflow_sla_totals
    fields: [findings_workflow_sla_totals.average_days_to_complete_total_workflow]
    view_label: "Assessment Findings"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_findings.assessment_findings_id} = ${findings_workflow_sla_totals.object_id} ;;
  }

  join: vendor_tiering_workflow_sla_totals {
    from: workflow_sla_totals
    fields: []
    type: left_outer
    relationship: one_to_one
    sql_on: ${services.service_id} = ${vendor_tiering_workflow_sla_totals.object_id} ;;
  }

  join: finding_assigned {
    from: users
    fields: []
    relationship: many_to_one
    sql_on: ${assessment_findings.assigned_to_id} = ${finding_assigned.user_id} ;;
  }

  join: service_assigned {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${services.assigned_to_id} = ${service_assigned.user_id} ;;
  }

  join: service_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${services.created_by_id} = ${service_created.user_id} ;;
  }

  join: service_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${services.submitted_by_id} = ${service_submitted.user_id} ;;
  }

  join: service_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${services.updated_by_id} = ${service_updated.user_id} ;;
  }

  join: procurements {
    from: processes
    type: left_outer
    relationship: many_to_one
    sql_on: ${all_objects.vendor_id} = ${procurements.vendor_id} ;;
  }

  join: procurement_criticality{
    from: severity
    fields: [procurement_criticality.criticality]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.severity_id} = ${procurement_criticality.severity_id} AND ${procurement_criticality.severity_type} = 'processes-severity';;
  }

  join: procurement_workflow_complete {
    from: workflow_complete
    view_label: "Procurements"
    fields: []
    relationship: many_to_one
    sql_on: ${procurements.process_id} = ${procurement_workflow_complete.object_id} ;;
  }


  join: procurement_business_department {
    from: business_department
    view_label: "Procurements"
    fields: [procurement_business_department.business_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.business_department} = ${procurement_business_department.business_department_id} ;;
  }

  join: procurements_technical_department {
    from: technical_department
    view_label: "Procurements"
    fields: [procurements_technical_department.technical_department]
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.technical_department} = ${procurements_technical_department.technical_department_id} ;;
  }

  join: procurements_subsidiary {
    from: subsidiary
    view_label: "Procurements"
    fields: [procurements_subsidiary.subsidiary]
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.subsidiary} = ${procurements_subsidiary.subsidiary_id} ;;
  }

  join: process_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.created_by_id} = ${process_created.user_id} ;;
  }

  join: procurement_status {
    from: status
    fields: [procurement_status.status]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.status_id} = ${procurement_status.status_id} ;;
  }

  join: procurement_severity {
    from: severity
    fields: [procurement_severity.severity]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.severity_id} = ${procurement_severity.severity_id} ;;
  }

  join: procurement_class {
    from: class
    fields: [procurement_class.class]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.class_id} = ${procurement_class.class_id} ;;
  }

  join: process_workflow_sla_totals {
    from: workflow_sla_totals
    fields: [process_workflow_sla_totals.average_days_to_complete_total_workflow]
    view_label: "Procurements"
    type: left_outer
    relationship: many_to_one
    sql_on: ${procurements.process_id} = ${process_workflow_sla_totals.object_id} ;;
  }

}




#############################################################################Assessments


explore: assessments {

  join: assessment_status {
    from: status
    view_label: "Assessments"
    fields: [assessment_status.status]
    relationship: many_to_one
    sql_on: ${assessments.status_id} = ${assessment_status.status_id} ;;
  }

  join: assessment_workflow_complete {
    from: workflow_complete
    view_label: "Assessments"
    fields: []
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_complete.object_id} ;;
  }

  join: assessment_severity {
    from: severity
    view_label: "Assessments"
    fields: [assessment_severity.severity]
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_severity.severity_id} ;;
  }

  join: assessment_criticality{
    from: severity
    fields: [assessment_criticality.criticality]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_criticality.severity_id} AND ${assessment_criticality.severity_type} = 'assessments-severity';;
  }

  join: vendors {
    relationship: one_to_many
    sql_on: ${assessments.vendor_id} = ${vendors.vendor_id} ;;
  }

  join: business_department {
    view_label: "Assessments"
    fields: [business_department.business_department]
    relationship: many_to_one
    sql_on: ${assessments.business_department} = ${business_department.business_department_id} ;;
  }

  join: technical_department {
    view_label: "Assessments"
    fields: [technical_department.technical_department]
    relationship: many_to_one
    sql_on: ${assessments.technical_department} = ${technical_department.technical_department_id} ;;
  }

  join: subsidiary {
    view_label: "Assessments"
    fields: [subsidiary.subsidiary]
    relationship: many_to_one
    sql_on: ${assessments.subsidiary} = ${subsidiary.subsidiary_id} ;;
  }

  join: solutions {
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.solution_id} = ${solutions.solution_id} ;;
  }

  join: assessment_assigned {
    view_label: "Assessments"
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assigned_to} = ${assessment_assigned.user_id} ;;
  }

  join: assessment_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.created_by_id} = ${assessment_created.user_id} ;;
  }

  join: assessment_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.submitted_by_id} = ${assessment_submitted.user_id} ;;
  }

  join: assessment_class {
    from: class
    fields: [assessment_class.class]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.class_id} = ${assessment_class.class_id} ;;
  }

  join: assessment_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.updated_by_id} = ${assessment_updated.user_id} ;;
  }

  join: survey_outcomes {
    view_label: "Survey"
    fields: []
    type: left_outer
    relationship: one_to_many
    sql_on: ${assessments.assessment_id} = ${survey_outcomes.survey_outcome_id} ;;
  }

  join: survey_answers {
    view_label: "Survey"
    fields: [survey_answers.answer_text]
    type: left_outer
    relationship: many_to_one
    sql_on: ${survey_outcomes.answer_id} = ${survey_answers.answer_id} ;;
  }

  join: survey_questions {
    view_label: "Survey"
    fields: [survey_questions.title, survey_questions.question_text, survey_questions.question_number]
    type: left_outer
    relationship: many_to_one
    sql_on: ${survey_outcomes.question_id} = ${survey_questions.question_id} ;;
  }

  join: survey_outcome_comments {
    view_label: "Survey"
    fields: [survey_outcome_comments.comment]
    type: left_outer
    relationship: many_to_one
    sql_on: ${survey_outcomes.survey_instance_id} = ${survey_outcome_comments.survey_instance_id} AND ${survey_outcomes.question_id} = ${survey_outcome_comments.question_id};;
  }

  join: vendor_severity {
    from: severity
    fields: [vendor_severity.severity]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.severity_id} = ${vendor_severity.severity_id} ;;
  }

  join: assessment_workflow {
    view_label: "Workflow"
    type: left_outer
    relationship: one_to_many
    sql_on: ${assessments.assessment_id} = ${assessment_workflow.object_id} ;;
  }

  join: assignee {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_workflow_sla.step_assigned_to} = ${assignee.user_id} ;;
  }

  join: step_user {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_workflow.step_user_id} = ${step_user.user_id} ;;
  }

  join: assessment_workflow_sla {
    from: assessment_workflow_sla_test
    view_label: "SLA"
    type: left_outer
    relationship: one_to_many
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_sla.object_id} ;;
  }

  join: assessment_current_workflow {
    view_label: "Current Workflow"
    type: left_outer
    relationship: one_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_current_workflow.object_id} ;;
  }

  join: current_workflow_status {
    from: status
    fields: [current_workflow_status.status]
    view_label: "Current Workflow"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_current_workflow.status_id} = ${current_workflow_status.status_id} ;;
  }

  join: current_workflow_assignee {
    view_label: "Current Workflow"
    from: users
    fields: [current_workflow_assignee.full_name]
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessment_current_workflow.assignee_id} = ${current_workflow_assignee.user_id} ;;
  }

  join: assessments_resolution {
    from:  resolutions
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.resolution_id} = ${assessments_resolution.resolution_id} ;;
  }

  join: vendor_class {
    from: class
    fields: [vendor_class.class]
    view_label: "Vendors"
    type: left_outer
    relationship: many_to_one
    sql_on: ${vendors.class_id}=${vendor_class.class_id} ;;
  }

  join: applications {
    type: left_outer
    relationship: one_to_many
    sql_on: ${vendors.vendor_id} = ${applications.vendor_id} ;;
  }

  join: applications_criticality{
    from: severity
    fields: [applications_criticality.criticality]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${applications_criticality.severity_id} AND ${applications_criticality.severity_type} = 'applications-severity';;
  }

  join: application_workflow_complete {
    from: workflow_complete
    view_label: "Applications"
    fields: []
    relationship: many_to_one
    sql_on: ${applications.application_id} = ${application_workflow_complete.object_id} ;;
  }

  join: application_class {
    from: class
    fields: [application_class.class]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.class_id} = ${application_class.class_id} ;;
  }

  join: application_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.created_by_id} = ${application_created.user_id} ;;
  }

  join: application_resolution {
    from: resolutions
    fields: [application_resolution.resolution]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.resolution_id} = ${application_resolution.resolution_id} ;;
  }

  join: application_severity {
    from: severity
    fields: [application_severity.severity]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${application_severity.severity_id} ;;
  }

  join: application_status {
    from: status
    fields: [application_status.status]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.status_id} = ${application_status.status_id} ;;
  }

  join: application_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.updated_by_id} = ${application_updated.user_id} ;;
  }

  join: assessment_workflow_sla_test {
    from: workflow_sla_totals
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_sla_test.object_id} ;;
  }
}

#############################################################################     EXPLORE Vulnerabilities
explore: vulnerabilities {
  join: status {
    view_label: "Vulnerabilities"
    fields: [status.status]
    type:  left_outer
    relationship: many_to_one
    sql_on: ${vulnerabilities.status_id} = ${status.status_id} ;;
  }

  join: resolutions {
    view_label: "Vulnerabilities"
    fields: [resolutions.resolution]
    type:  left_outer
    relationship: many_to_one
    sql_on: ${vulnerabilities.resolution_id} = ${resolutions.resolution_id} ;;
  }
}

#############################################################################     EXPLORE Findings
explore:  findings{
  join: resolutions {
    view_label: "Findings"
    fields: [resolutions.resolution]
    type:  left_outer
    relationship: many_to_one
    sql_on: ${findings.resolution} = ${resolutions.resolution_id} ;;
  }
}

#############################################################################  EXPLORE Applications / Products

explore: applications {
  label: "Products"
  view_label: "Products"

  join: applications_criticality{
    from: severity
    fields: [applications_criticality.criticality]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${applications_criticality.severity_id} AND ${applications_criticality.severity_type} = 'applications-severity';;
  }

  join: application_class {
    from: class
    fields: [application_class.class]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.class_id} = ${application_class.class_id} ;;
  }

  join: application_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.created_by_id} = ${application_created.user_id} ;;
  }

  join: application_resolution {
    from: resolutions
    fields: [application_resolution.resolution]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.resolution_id} = ${application_resolution.resolution_id} ;;
  }

  join: application_severity {
    from: severity
    fields: [application_severity.severity]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.severity_id} = ${application_severity.severity_id} ;;
  }

  join: application_status {
    from: status
    fields: [application_status.status]
    view_label: "Products"
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.status_id} = ${application_status.status_id} ;;
  }

  join: application_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${applications.updated_by_id} = ${application_updated.user_id} ;;
  }

  join: application_workflow_complete {
    from: workflow_complete
    view_label: "Products"
    fields: []
    relationship: many_to_one
    sql_on: ${applications.application_id} = ${application_workflow_complete.object_id} ;;
  }

  join: assessments {
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.application_id} = ${applications.application_id} ;;
  }

  join: assessment_workflow_complete {
    from: workflow_complete
    view_label: "Assessments"
    fields: []
    relationship: many_to_one
    sql_on: ${assessments.assessment_id} = ${assessment_workflow_complete.object_id} ;;
  }

  join: assessment_assigned {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.assigned_to_id} = ${assessment_assigned.user_id} ;;
  }

  join: assessment_class {
    from: class
    fields: [assessment_class.class]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.class_id} = ${assessment_class.class_id} AND ${assessment_class.class_type} = 'assessments-class';;
  }

  join: assessment_created {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.created_by_id} = ${assessment_created.user_id} ;;
  }

  join: assessments_resolution {
    from: resolutions
    fields: [assessments_resolution.resolution]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.resolution_id} = ${assessments_resolution.resolution_id} AND ${assessments_resolution.resolution_type} = 'assessments-resolution';;
  }

  join: assessment_status {
    from: status
    fields: [assessment_status.status]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.status_id} = ${assessment_status.status_id} AND ${assessment_status.status_type} = 'assessments-status';;
  }

  join: assessment_severity {
    from: severity
    fields: [assessment_severity.severity]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_severity.severity_id} AND ${assessment_severity.severity_type} = 'assessments-severity';;
  }

  join: assessment_criticality{
    from: severity
    fields: [assessment_criticality.criticality]
    view_label: "Assessments"
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.severity_id} = ${assessment_criticality.severity_id} AND ${assessment_criticality.severity_type} = 'assessments-severity';;
  }

  join: assessment_submitted {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.submitted_by_id} = ${assessment_submitted.user_id} ;;
  }

  join: assessment_updated {
    from: users
    fields: []
    type: left_outer
    relationship: many_to_one
    sql_on: ${assessments.updated_by_id} = ${assessment_updated.user_id} ;;
  }

}
#############################################################################  EXPLORE FIA

explore: fia_overview {}
