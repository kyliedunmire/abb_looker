view: findings {
  sql_table_name: abb.findings ;;

  dimension: finding_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."finding_id" ;;
  }

  dimension: assessment_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assessment_id" ;;
  }

  dimension: assigned_to {
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: business_department {
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension_group: closed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."closed_date" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: cybersecurity_framework_category {
    type: string
    sql: ${TABLE}."cybersecurity_framework_category" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: finding_description {
    type: string
    sql: ${TABLE}."finding_description" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."finding_name" ;;
    html: <a href="https://fis-mtn.fortressis.com/cyberSecurity/findings/edit/{{ findings.finding_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: finding_notes {
    type: string
    sql: ${TABLE}."finding_notes" ;;
  }

  dimension: finding_severity_id {
    hidden: yes
    type: string
    sql: ${TABLE}."finding_severity_id" ;;
  }

  dimension: finding_type {
    type: string
    sql: ${TABLE}."finding_type" ;;
  }

  dimension: impact {
    type: string
    sql: ${TABLE}."impact" ;;
  }

  dimension: iso_mapping {
    type: string
    sql: ${TABLE}."iso_mapping" ;;
  }

  dimension: likelihood {
    type: string
    sql: ${TABLE}."likelihood" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: recommendation {
    type: string
    sql: ${TABLE}."recommendation" ;;
  }

  dimension: resolution {
    hidden: yes
    type: string
    sql: ${TABLE}."resolution" ;;
  }

  dimension: speed_and_onset {
    type: string
    sql: ${TABLE}."speed_and_onset" ;;
  }

  dimension: status_id {
    hidden: yes
    type: string
    sql: ${TABLE}."status_id" ;;
  }

  dimension: subsidiary {
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: technical_department {
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: vendor_id {
    hidden: yes
    type: string
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: vulnerability {
    type: string
    sql: ${TABLE}."vulnerability" ;;
  }

  measure: count {
    type: count
    drill_fields: [details*]
  }

  measure: running_total {
    type: running_total
    sql: ${count} ;;
    direction: "column"
    drill_fields: [details*]
  }

  set: details {
    fields: [
      platform_id,
      name,
      finding_description,
      finding_notes,
      finding_type
    ]
  }
}
