view: resolutions {
  sql_table_name: abb.resolutions ;;
  drill_fields: [resolution_id]

  dimension: resolution_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."resolution_id" ;;
  }

  dimension: resolution {
    type: string
    sql: ${TABLE}."resolution_label" ;;
  }

  dimension: resolution_type {
    type: string
    sql: ${TABLE}."resolution_type" ;;
  }

  measure: count {
    type: count
    drill_fields: [resolution_id, applications.count, assessments.count, engagement_scopes.count]
  }
}
