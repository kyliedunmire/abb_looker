view: survey_outcome_comments_internal {
  sql_table_name: abb.survey_outcome_comments_internal ;;

  dimension: comment {
    type: string
    sql: ${TABLE}."comment" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: question_id {
    type: string
    sql: ${TABLE}."question_id" ;;
  }

  dimension: question_number {
    type: string
    sql: ${TABLE}."question_number" ;;
  }

  dimension: survey_instance_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."survey_instance_id" ;;
  }

  dimension: survey_outcome_id {
    type: string
    sql: ${TABLE}."survey_outcome_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [survey_instances.survey_instance_id]
  }
}
