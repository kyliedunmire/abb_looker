view: processes {
  sql_table_name: abb.processes ;;

  dimension: process_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."process_id" ;;
  }

  dimension: application_id {
    hidden: yes
    type: string
    sql: ${TABLE}."application_id" ;;
  }

  dimension: asset_id {
    hidden: yes
    type: string
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}."class_id" ;;
  }

  dimension_group: complete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:${procurement_workflow_complete.date_raw};;
  }

  dimension: contract_id {
    hidden: yes
    type: string
    sql: ${TABLE}."contract_id" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: created_by {
    group_label: "Created By"
    group_item_label: "Name"
    type: string
    sql: ${process_created.full_name} ;;
  }

  dimension: created_by_email {
    group_label: "Created By"
    group_item_label: "Email"
    type: string
    sql: ${process_created.email} ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."process_name" ;;
    html: <a href="https://fis-mtn.fortressis.com/processes/details/{{ procurements.process_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."process_type" ;;
  }

  dimension: resolution_id {
    hidden: yes
    type: string
    sql: ${TABLE}."resolution_id" ;;
  }

  dimension: severity_id {
    hidden: yes
    type: string
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    hidden: yes
    type: string
    sql: ${TABLE}."status_id" ;;
  }

  dimension_group: submitted {
    type: time
    sql: ${TABLE}.submitted_date ;;
    timeframes: [
      date,
      week,
      month,
      year
    ]
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: vendor_id {
    hidden: yes
    type: string
    sql: ${TABLE}."vendor_id" ;;
  }

  ##########-------------------------------------------------------------------------------------------------------------------

  measure: count {
    type: count
    drill_fields: [details*]
  }

  measure: count_of_open_procurements {
    type: count
    filters: {
      field: procurement_status.status
      value: "-Submitted, -Closed, -Complete, -Termination"
    }
    drill_fields: [details*]
  }

  measure: count_of_closed_procurements{
    type: count
    filters: {
      field:procurement_status.status
      value: "Submitted, Closed, Complete, Termination"
    }
    drill_fields: [details*]
  }

  ##########---------------------------------------------------------------------------------------------------------------------

  set: details {
    fields: [      platform_id,
      name,
      description,
      procurement_status.status,
      procurement_severity.severity,
      vendors.count,
      assessments.count,
      solutions.count
      ]
  }
}
