view: applications {
  sql_table_name: abb.applications ;;
  drill_fields: [application_id]

  dimension: application_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."application_id" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."application_name" ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}."category" ;;
  }

  dimension: cip_compliance {
    label: "CIP Compliance"
    type: yesno
    sql: ${TABLE}."cip_compliance" ;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}."class_id" ;;
  }

  dimension_group: complete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CASE
          WHEN ${application_workflow_complete.date_raw} IS NULL AND ${submitted_date} IS NULL and ${status_id} = 'application_stat_term'
          THEN ${updated_date}
          WHEN ${application_workflow_complete.date_raw} IS NULL and ${status_id} = 'application_stat_term'
          THEN ${submitted_date}
          WHEN ${status_id} = 'application_stat_term'
          THEN ${application_workflow_complete.date_raw}
                END;;
  }


  dimension: compliance_risk {
    type: string
    sql: ${TABLE}."compliance_risk" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: created_by {
    group_label: "Created By"
    group_item_label: "Name"
    type: string
    sql: ${application_created.full_name} ;;
  }

  dimension: created_by_email {
    group_label: "Created By"
    group_item_label: "Email"
    type: string
    sql: ${application_created.email} ;;
  }

  dimension: impact {
    type: string
    sql: ${TABLE}."impact" ;;
  }

  dimension: is_closed {
    type: yesno
    sql: ${TABLE}."is_closed" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: related_asset {
    hidden: yes
    type: string
    sql: ${TABLE}."related_asset" ;;
  }

  dimension_group: reported {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."reported_date" ;;
  }

  dimension: resolution_id {
    type: string
    hidden: yes
    sql: ${TABLE}."resolution_id" ;;
  }

  dimension: severity_id {
    type: string
    hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension_group: submitted {
    type: time
    sql: ${TABLE}.submitted_date ;;
    timeframes: [
      date,
      week,
      month,
      year
    ]
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: updated_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."updated_by" ;;
  }

  dimension: updated_by {
    group_label: "Updated By"
    group_item_label: "Name"
    type: string
    sql: ${application_updated.full_name} ;;
  }

  dimension: updated_by_email {
    group_label: "Updated By"
    group_item_label: "Email"
    type: string
    sql: ${application_updated.email} ;;
  }

  dimension: process_id {
    hidden: yes
    type: string
    sql: ${TABLE}.process_id ;;
  }

  dimension: vendor_id {
    hidden: yes
    type: string
    sql: ${TABLE}.vendor_id ;;
  }

  ####--------------------------------------------------------------------------------------------------------------------------------------------------------------------

  measure: count {
    type: count_distinct
    sql: ${TABLE}.application_id ;;
    drill_fields: [platform_id,
      name,
      application_status.status, applications_criticality.criticality]
  }

  measure: assessed {
    type: count_distinct
    sql: ${assessments.vendor_id} ;;
    drill_fields: [platform_id,
      name,
      application_status.status, applications_criticality.criticality, assessments.name, assessments.type]
  }

  measure: count_of_open_products {
    type: count
    filters: {
      field: application_status.status
      value: "-Submitted, -Closed, -Complete, -Termination"
    }
    drill_fields: [platform_id,
      name, application_status.status,
      applications_criticality.criticality]
  }

  measure: count_of_closed_products{
    type: count
    filters: {
      field:application_status.status
      value: "Submitted, Closed, Complete, Termination"
    }
    drill_fields: [platform_id,
      name, application_status.status,
      application_criticality.criticality]
  }


  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      platform_id,
      name
    ]
  }
}
