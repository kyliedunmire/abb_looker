view: procurement_path_questionnaire {
  derived_table: {
    sql: SELECT p.process_id,
            p.process_name,
            sq.question_text,
            sa.answer_text,
            soc.comment,
            so.survey_instance_id,
            sq.qid                                       as question_number,
            sq.section_title                             as title,
            sq.question_weight,
            sa.answer_weight,
            (sq.question_weight * sa.answer_weight)::int as score
            FROM abb.processes p
              LEFT JOIN abb.survey_outcomes_internal so
                        ON so.survey_outcome_id = p.process_id AND
                           (so.survey_template_id = '9aRCp7wWFGayCNHoE')
              LEFT JOIN (
                    SELECT DISTINCT ON (question_id) question_id,
                                          section_title,
                                          qid,
                                          question_text,
                                          question_weight,
                                          section_index,
                                          bo_id,
                                          survey_instance_id
                    FROM abb.survey_object_internal) sq ON sq.question_id::text = so.question_id::text
              LEFT JOIN (
                    SELECT answer_id, answer_text, answer_weight, question_id, survey_instance_id
                    FROM abb.survey_object_internal) sa ON so.answer_id::text = sa.answer_id::text AND sq.question_id = sa.question_id AND sq.survey_instance_id = sa.survey_instance_id
              LEFT JOIN (
                    SELECT *
                    FROM abb.survey_outcome_comments_internal) soc ON soc.question_id = sq.question_id
            ORDER BY sq.section_index, question_number
 ;;
  }

  measure: answers {
    type: list
    list_field: answer
  }

  #############-----------------------------------------------------------------------------------


  dimension: process_id {
    hidden: yes
    type: string
    sql: ${TABLE}."process_id" ;;
  }

  dimension: process_name {
    hidden: yes
    type: string
    sql: ${TABLE}."process_name" ;;
  }

  dimension: question {
    type: string
    sql: ${TABLE}."question_text" ;;
  }

  dimension: answer {
    type: string
    sql: ${TABLE}."answer_text" ;;
  }

  dimension: comment {
    type: string
    sql: ${TABLE}."comment" ;;
  }

  dimension: survey_instance_id {
    hidden: yes
    type: string
    sql: ${TABLE}."survey_instance_id" ;;
  }

  dimension: question_number {
    type: string
    sql: ${TABLE}."question_number" ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}."title" ;;
  }

  dimension: question_weight {
    hidden: yes
    type: number
    sql: ${TABLE}."question_weight" ;;
  }

  dimension: answer_weight {
    hidden: yes
    type: number
    sql: ${TABLE}."answer_weight" ;;
  }

  dimension: score {
    type: number
    sql: ${TABLE}."score" ;;
  }

  set: detail {
    fields: [
      process_id,
      process_name,
      question,
      answer,
      comment,
      survey_instance_id,
      question_number,
      title,
      question_weight,
      answer_weight,
      score
    ]
  }
}
