view: assessments {
  sql_table_name: abb.assessments ;;

  dimension: assessment_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."assessment_id" ;;
  }

  dimension: application_id {
    type: string
    hidden: yes
    sql: (${TABLE}."application_id")->>0 ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."assessment_name" ;;
    html: <a href="https://fis-mtn.fortressis.com/assessments/details/{{ assessments.assessment_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}."assessment_notes" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."assessment_type" ;;
  }

  dimension: assigned_to_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: assigned_to {
    group_label: "Assigned To"
    group_item_label: "Name"
    type: string
    sql: ${assessment_assigned.full_name} ;;
  }

  dimension: assigned_to_email {
    group_label: "Assigned To"
    group_item_label: "Email"
    type: string
    sql: ${assessment_assigned.email} ;;
  }

  dimension: business_department {
    hidden: yes
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension: cip_compliance {
    type: yesno
    sql: ${TABLE}."cip_compliance" ;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}."class_id" ;;
  }

  dimension_group: complete {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CASE
          WHEN ${assessment_workflow_complete.date_raw} IS NULL AND ${submitted_date} IS NULL and (${status_id} = 'qwki3M2nXuwc4So4c' OR ${status_id} = 'xS5ninvXeoeLseQDH')
          THEN ${modified_date}
          WHEN ${assessment_workflow_complete.date_raw} IS NULL and (${status_id} = 'qwki3M2nXuwc4So4c' OR ${status_id} = 'xS5ninvXeoeLseQDH')
          THEN ${submitted_date}
          WHEN (${status_id} = 'qwki3M2nXuwc4So4c' OR ${status_id} = 'xS5ninvXeoeLseQDH')
          THEN ${assessment_workflow_complete.date_raw}
                END;;
  }

  dimension: compliance_risk {
    type: string
    sql: ${TABLE}."compliance_risk" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: created_by {
    group_label: "Created By"
    group_item_label: "Name"
    type: string
    sql: ${assessment_created.full_name} ;;
  }

  dimension: created_by_email {
    group_label: "Created By"
    group_item_label: ""
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension: disposition_required {
    type: yesno
    sql: ${TABLE}."disposition_required" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."modified_at" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: related_assessment {
    hidden: yes
    type: string
    sql: ${TABLE}."related_assessment" ;;
  }

  dimension: related_asset {
    hidden: yes
    type: string
    sql: ${TABLE}."related_asset" ;;
  }

  dimension: related_service {
    hidden: yes
    type: string
    sql: ${TABLE}."related_service" ;;
  }

  dimension: resolution_id {
    type: string
    hidden: yes
    sql: ${TABLE}."resolution_id" ;;
  }

  dimension: severity_id {
    type: string
    hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: solution_id {
    type: string
    hidden: yes
    sql: ${TABLE}."solution_id" ;;
  }

  dimension: status_id {
    type: string
    hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension: submitted_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."submitted_by" ;;
  }

  dimension: submitted_by {
    group_label: "Submitted By"
    group_item_label: "Name"
    type: string
    sql: ${assessment_submitted.full_name} ;;
  }

  dimension: submitted_by_email {
    group_label: "Submitted By"
    group_item_label: "Email"
    type: string
    sql: ${assessment_submitted.email} ;;
  }

  dimension_group: submitted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."submitted_date" ;;
  }

  dimension: subsidiary {
    hidden: yes
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}."tags" ;;
  }

  dimension: technical_department {
    hidden: yes
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: updated_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."updated_by" ;;
  }

  dimension: updated_by {
    group_label: "Updated By"
    group_item_label: "Name"
    type: string
    sql: ${assessment_updated.full_name} ;;
  }

  dimension: updated_by_email {
    group_label: "Updated By"
    group_item_label: "Email"
    type: string
    sql: ${assessment_updated.email} ;;
  }

  dimension: vendor_id {
    type: string
    hidden: yes
    sql: ${TABLE}."vendor_id" ;;
  }

  dimension: process_id {
    hidden: yes
    type: string
    sql: ${TABLE}.process_id ;;
  }

  dimension: coming_due_past_due_general {
    label: "Coming Due/Past Due (General)"
    group_label: "Coming Due/Past Due"
    group_item_label: "General"
    type: string
    sql: CASE
         WHEN ${status_id} IN ('wfWjLQ6JfxjB6nyCQ','EoW3XC3WZ5n9428Ak')
         THEN 'Closed'
         WHEN extract(days from (${due_date} - now())) < 0
         THEN 'Past due'
         WHEN extract(days from (${due_date} - now())) < 7
         THEN 'Coming due'
         ELSE 'No escalation required'
         END;; #There are 2 assessment finding status documents, bringing in the back end ID of both just in case
    description: "Generalizing all incomplete Assessments as either past due or coming due within 2 days"
  }

  dimension: coming_due_past_due_detailed {
    label: "Coming Due/Past Due (Detailed)"
    group_label: "Coming Due/Past Due"
    group_item_label: "Detailed"
    type: string
    sql: CASE
          WHEN ${status_id} IN ('wfWjLQ6JfxjB6nyCQ','EoW3XC3WZ5n9428Ak')
          THEN 'Closed'
          WHEN extract(days from (${due_date} - now())) < -6
          THEN '>7 days past due'
          WHEN extract(days from (${due_date} - now())) < 0
          THEN '<7 days past due'
          WHEN extract(days from (${due_date} - now())) < 7
          THEN '<7 days coming due'
          ELSE '>7 days coming due'
          END;; #There are 2 assessment finding status documents, bringing in the back end ID of both just in case
    description: "Providing details about the state of coming due/past due - More than a week past due, within a week past due, coming due within a week, coming due within more than a week."
  }

############---------------------------------------------------------------------------------------------------------------------------

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: count_of_open_assessments {
    type: count
    filters: {
      field: assessment_status.status
      value: "-Submitted, -Closed, -Complete, -Termination"
    }
    drill_fields: [detail*]
  }

  measure: count_of_closed_assessments{
    type: count
    filters: {
      field:assessment_status.status
      value: "Submitted, Closed, Complete, Termination"
    }
    drill_fields: [detail*]
  }


  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      platform_id,
      name,
      assessment_severity.severity,
      assessment_status.status,
      description,
      notes,
      assigned_to
    ]
  }
}
