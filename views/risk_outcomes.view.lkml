view: risk_outcomes {
  sql_table_name: abb.risk_outcomes ;;
  drill_fields: [risk_outcome_id]

  dimension: risk_outcome_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."risk_outcome_id" ;;
  }

  dimension: assessment_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."assessment_id" ;;
  }

  dimension: asset_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: assigned_to {
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: business_department {
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: finding_id {
    type: string
    sql: ${TABLE}."finding_id" ;;
  }

  dimension: outcome_description {
    type: string
    sql: ${TABLE}."outcome_description" ;;
  }

  dimension: outcome_name {
    type: string
    sql: ${TABLE}."outcome_name" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: severity_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: status_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."status_id" ;;
  }

  dimension_group: submitted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."submitted_date" ;;
  }

  dimension: subsidiary {
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: technical_department {
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      risk_outcome_id,
      outcome_name,
      status.status_id,
      assessments.assessment_id,
      assessments.assessment_name,
      assets.asset_name,
      assets.asset_id,
      severity.severity_id,
      tasks.count
    ]
  }
}
