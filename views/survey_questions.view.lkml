view: survey_questions {
  sql_table_name: abb.survey_questions ;;

  dimension: question_id {
    type: string
    sql: ${TABLE}."question_id" ;;
  }

  dimension: question_instructions {
    type: string
    sql: ${TABLE}."question_instructions" ;;
  }

  dimension: question_number {
    type: string
    sql: ${TABLE}."question_number" ;;
  }

  dimension: question_text {
    type: string
    sql: ${TABLE}."question_text" ;;
  }

  dimension: section_index {
    type: number
    sql: ${TABLE}."section_index" ;;
  }

  dimension: survey_template_id {
    type: string
    sql: ${TABLE}."survey_template_id" ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}."title" ;;
  }

  dimension: weight {
    type: number
    sql: ${TABLE}."weight" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
