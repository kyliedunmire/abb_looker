view: severity {
  sql_table_name: abb.severity ;;
  drill_fields: [severity_id]

  dimension: severity_id {
    primary_key: yes
    type: string
    sql: ${TABLE}."severity_id" ;;
  }

  dimension: severities {
    hidden: yes
    type: string
    sql: ${TABLE}."severity_label" ;;
  }

  dimension: severity {
    type: string
    case: {
      when: {
        sql: ${severities} = 'High' ;;
        label: "High"
      }
      when: {
        sql: ${severities} = 'Moderate' ;;
        label: "Moderate"
      }
      when: {
        sql: ${severities} = 'Low' ;;
        label: "Low"
      }
    }
  }

  dimension: criticality {
    type: string
    case: {
      when: {
        sql: ${severities} = 'Critical' ;;
        label: "Critical"
      }
      when: {
        sql: ${severities} = 'High' ;;
        label: "High"
      }
      when: {
        sql: ${severities} = 'Moderate' OR  ${severities} = 'Medium'  ;;
        label: "Moderate"
      }
      when: {
        sql: ${severities} = 'Low' OR ${severities} = 'Nominal'  ;;
        label: "Low"
      }
      when: {
        sql: ${severities} = 'Not Ranked' OR ${severities} is null OR ${severities} = '' OR ${severities} = 'Unknown';;
        label: "Not Ranked"
      }
    }
  }

  dimension: severity_type {
    type: string
    sql: ${TABLE}."severity_type" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      severity_id,
      applications.count,
      assessment_findings.count,
      assessments.count,
      assets.count,
      detected_vulnerabilities.count,
      engagement_scopes.count,
      risk_outcomes.count,
      services.count,
      tasks.count,
      vendors.count
    ]
  }
}
