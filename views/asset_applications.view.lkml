view: asset_applications {
  sql_table_name: abb.asset_applications ;;

  dimension: applications_id {
    type: string
    hidden: yes
    sql: ${TABLE}."applications_id" ;;
  }

  dimension: asset_id {
    type: string
    hidden: yes
    sql: ${TABLE}."asset_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [assets.asset_name, assets.asset_id, applications.application_name, applications.application_id]
  }
}
