view: application_versions {
  sql_table_name: abb.application_versions ;;

  dimension: application_id {
    type: string
    hidden: yes
    sql: ${TABLE}."application_id" ;;
  }

  dimension: asset_id {
    type: string
    hidden: yes
    sql: ${TABLE}."asset_id" ;;
  }

  dimension: version_number {
    type: string
    sql: ${TABLE}."version_number" ;;
  }

  measure: count {
    type: count
    drill_fields: [assets.asset_name, assets.asset_id, applications.application_name, applications.application_id]
  }
}
