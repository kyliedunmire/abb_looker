view: procurement_qa {
  derived_table: {
    sql: SELECT survey_outcome_id,
       survey_outcomes_interal."PPQ01" AS "CIP",
       survey_outcomes_interal."PPQ02" AS "Remote Access",
       survey_outcomes_interal."PPQ03" AS "Sensitive Information"
FROM public.crosstab('select survey_outcome_id, qid, string_agg(answer_text, '','' ) as answer from abb.survey_outcomes_internal so LEFT JOIN (
                    SELECT DISTINCT ON (question_id) question_id,
                                          section_title,
                                          qid,
                                          question_text,
                                          question_weight,
                                          section_index,
                                          bo_id,
                                          survey_instance_id
                    FROM abb.survey_object_internal) sq ON sq.question_id::text = so.question_id::text
              LEFT JOIN (
                    SELECT answer_id, answer_text, answer_weight, question_id, survey_instance_id
                    FROM abb.survey_object_internal) sa ON so.answer_id::text = sa.answer_id::text AND sq.question_id = sa.question_id AND sq.survey_instance_id = sa.survey_instance_id WHERE so.survey_template_id = ''9aRCp7wWFGayCNHoE'' GROUP BY 1,2 ORDER BY 1,2') survey_outcomes_interal (survey_outcome_id varchar, "PPQ01" text, "PPQ02" text, "PPQ03" text)
 ;;
  }

  dimension: survey_outcome_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."survey_outcome_id" ;;
  }

  dimension: cip {
    group_label: "Procurement Risk Determination"
    label: "CIP"
    type: string
    sql: ${TABLE}."CIP" ;;
  }

  dimension: remote_access {
    group_label: "Procurement Risk Determination"
    type: string
    label: "Remote Access"
    sql: ${TABLE}."Remote Access" ;;
  }

  dimension: sensitive_information {
    group_label: "Procurement Risk Determination"
    type: string
    label: "Sensitive Information"
    sql: ${TABLE}."Sensitive Information" ;;
  }

  ############----------------------------------------------------------------------------------------------------

  measure: count_cip {
    type: count
    filters: {
      field: cip
      value: "Yes"
    }
    drill_fields: [detail*]
  }

  ###########--------------------------------------------------------------------------------------------------------

  set: detail {
    fields: [procurements.platform_id,
      procurements.name,
      procurements.type,
      procurement_status.status,
      procurements.description,
      cip,
      remote_access,
      sensitive_information]
  }
}
