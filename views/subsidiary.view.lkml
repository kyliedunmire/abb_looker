view: subsidiary {
  derived_table: {
    sql: SELECT *
    FROM abb.vendor_department
    WHERE vendor_department_type = 'subsidiary';;
  }

  dimension: subsidiary_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.vendor_department_id ;;
  }

  dimension: subsidiary {
    group_label: "Business Units"
    type: string
    sql: ${TABLE}.vendor_department_name ;;
  }
}
