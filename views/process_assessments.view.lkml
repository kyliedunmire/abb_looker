view: process_assessments {
  sql_table_name: abb.process_assessments ;;

  dimension: assessment_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assessment_id" ;;
  }

  dimension: process_id {
    hidden: yes
    type: string
    sql: ${TABLE}."process_id" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
