view: survey_answers {
  sql_table_name: abb.survey_answers ;;

  dimension: answer_id {
    type: string
    sql: ${TABLE}."answer_id" ;;
  }

  dimension: answer_text {
    type: string
    sql: ${TABLE}."answer_text" ;;
  }

  dimension: answer_weight {
    type: number
    sql: ${TABLE}."answer_weight" ;;
  }

  dimension: question_id {
    type: string
    sql: ${TABLE}."question_id" ;;
  }

  dimension: survey_template_id {
    type: string
    sql: ${TABLE}."survey_template_id" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
