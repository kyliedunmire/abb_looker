- dashboard: scrm_overview
  title: SCRM Overview
  layout: newspaper
  refresh: 30 seconds
  elements:
  - title: Vendor Types by Criticality
    name: Vendor Types by Criticality
    model: abb
    explore: vendors
    type: looker_grid
    fields: [vendors.count, vendors.type, vendor_criticality.criticality]
    pivots: [vendor_criticality.criticality]
    fill_fields: [vendor_criticality.criticality]
    sorts: [vendor_criticality.criticality desc, vendors.count desc 5]
    limit: 500
    total: true
    row_total: right
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    series_cell_visualizations:
      vendors.count:
        is_active: true
        palette:
          palette_id: cb8f900c-ffa8-aba1-d8a6-63d975c14ca5
          collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
          custom_colors:
          - "#91db31"
          - "#93de4e"
          - "#fad75f"
          - "#e6ac44"
          - "#c21613"
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      Not Ranked - 3 - vendors.count: "#736e75"
      Low - 2 - vendors.count: "#72D16D"
      Moderate - 1 - vendors.count: "#FFD95F"
      High - 0 - vendors.count: "#B32F37"
      Not Ranked - 4 - vendors.count: "#6e6b69"
      Low - 3 - vendors.count: "#72D16D"
      Moderate - 2 - vendors.count: "#FFD95F"
      High - 1 - vendors.count: "#E57947"
      Critical - 0 - vendors.count: "#B32F37"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#808080"
    column_order: []
    listen: {}
    row: 0
    col: 0
    width: 12
    height: 8
  - title: Product Types by Criticality
    name: Product Types by Criticality
    model: abb
    explore: applications
    type: looker_grid
    fields: [applications.count, applications.type, applications_criticality.criticality]
    pivots: [applications_criticality.criticality]
    fill_fields: [applications_criticality.criticality]
    sorts: [applications_criticality.criticality desc]
    limit: 500
    total: true
    row_total: right
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    series_cell_visualizations:
      applications.count:
        is_active: true
        palette:
          palette_id: 76a01620-bef4-1ffd-b045-a2f0f04b5dfd
          collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
          custom_colors:
          - "#96de5e"
          - "#C2DD67"
          - "#ebda52"
          - "#d99b3f"
          - "#c22410"
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      Not Ranked - 3 - applications.count: "#787671"
      Low - 2 - applications.count: "#72D16D"
      Moderate - 1 - applications.count: "#FFD95F"
      High - 0 - applications.count: "#B32F37"
      Not Ranked - 4 - applications.count: "#74756c"
      Low - 3 - applications.count: "#72D16D"
      Moderate - 2 - applications.count: "#FFD95F"
      High - 1 - applications.count: "#E57947"
      Critical - 0 - applications.count: "#B32F37"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#808080"
    column_order: []
    listen: {}
    row: 0
    col: 12
    width: 12
    height: 8
  - title: Assessed High Risk Vendors
    name: Assessed High Risk Vendors
    model: abb
    explore: vendors
    type: single_value
    fields: [vendors.assessed, vendors.total]
    filters:
      vendor_criticality.criticality: Critical,High
    limit: 500
    total: true
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: true
    comparison_type: progress_percentage
    comparison_reverse_colors: false
    show_comparison_label: true
    comparison_label: high risk vendors
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    listen: {}
    row: 11
    col: 0
    width: 12
    height: 4
  - title: FIA Total Files
    name: FIA Total Files
    model: abb
    explore: fia_overview
    type: single_value
    fields: [fia_overview.count]
    sorts: [fia_overview.count desc]
    limit: 500
    series_types: {}
    row: 17
    col: 12
    width: 6
    height: 5
  - title: FIA Files Verified
    name: FIA Files Verified
    model: abb
    explore: fia_overview
    type: single_value
    fields: [fia_overview.file_verified]
    limit: 500
    series_types: {}
    listen: {}
    row: 17
    col: 18
    width: 6
    height: 5
  - title: FIA Total Products
    name: FIA Total Products
    model: abb
    explore: fia_overview
    type: single_value
    fields: [fia_overview.product_count]
    filters:
      fia_overview.product: "-example"
    limit: 500
    series_types: {}
    listen: {}
    row: 22
    col: 18
    width: 6
    height: 5
  - title: FIA Total Vendors
    name: FIA Total Vendors
    model: abb
    explore: fia_overview
    type: single_value
    fields: [fia_overview.vendor_count]
    filters:
      fia_overview.vendor: "-demo"
    limit: 500
    series_types: {}
    listen: {}
    row: 22
    col: 12
    width: 6
    height: 5
  - title: Total Product Assessments
    name: Total Product Assessments
    model: abb
    explore: assessments
    type: single_value
    fields: [assessments.count]
    filters:
      assessments.type: Data-Driven Product Assessment,Comprehensive Product Assessment
    sorts: [assessments.count desc]
    limit: 500
    series_types: {}
    row: 43
    col: 0
    width: 6
    height: 4
  - title: Completed Product Assessments
    name: Completed Product Assessments
    model: abb
    explore: assessments
    type: single_value
    fields: [assessments.count_of_closed_assessments]
    filters:
      assessments.type: Data-Driven Product Assessment,Comprehensive Product Assessment
    limit: 500
    series_types: {}
    row: 39
    col: 0
    width: 6
    height: 4
  - title: Product Assessments by Status
    name: Product Assessments by Status
    model: abb
    explore: assessments
    type: looker_pie
    fields: [assessments.count, assessment_status.status]
    filters:
      assessments.type: Data-Driven Product Assessment,Comprehensive Product Assessment
    sorts: [assessments.count desc]
    limit: 500
    value_labels: legend
    label_type: labPer
    inner_radius: 60
    series_colors:
      Closed: "#72D16D"
      Scheduled: "#E57947"
    series_types: {}
    row: 39
    col: 6
    width: 9
    height: 8
  - title: Monthly Completed Product Assessments
    name: Monthly Completed Product Assessments
    model: abb
    explore: assessments
    type: looker_column
    fields: [assessments.count_of_closed_assessments, assessment_criticality.criticality,
      assessments.complete_month]
    pivots: [assessment_criticality.criticality]
    fill_fields: [assessment_criticality.criticality, assessments.complete_month]
    filters:
      assessments.type: Data-Driven Product Assessment,Comprehensive Product Assessment
    sorts: [assessment_criticality.criticality, assessments.complete_month desc]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      assessment_severity.severity___null - assessment_severity.severity__sort____null - assessments.count_of_closed_assessments: "#818279"
      Low - 2 - assessments.count_of_closed_assessments: "#72D16D"
      Moderate - 1 - assessments.count_of_closed_assessments: "#FFD95F"
      High - 0 - assessments.count_of_closed_assessments: "#B32F37"
      Not Ranked - 4 - assessments.count_of_closed_assessments: "#bcbdbf"
      Low - 3 - assessments.count_of_closed_assessments: "#72D16D"
      Moderate - 2 - assessments.count_of_closed_assessments: "#FFD95F"
      High - 1 - assessments.count_of_closed_assessments: "#E57947"
      Critical - 0 - assessments.count_of_closed_assessments: "#B32F37"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#808080"
    row: 39
    col: 15
    width: 9
    height: 8
  - title: Total Vendor Assessments
    name: Total Vendor Assessments
    model: abb
    explore: assessments
    type: single_value
    fields: [assessments.count]
    filters:
      assessments.type: Data-Driven Vendor Assessment,Vendor Reconnaissance Report
    sorts: [assessments.count desc]
    limit: 500
    series_types: {}
    row: 53
    col: 0
    width: 6
    height: 4
  - title: Completed Vendor Assessments
    name: Completed Vendor Assessments
    model: abb
    explore: assessments
    type: single_value
    fields: [assessments.count_of_closed_assessments]
    filters:
      assessments.type: Data-Driven Vendor Assessment,Vendor Reconnaissance Report
    limit: 500
    series_types: {}
    row: 49
    col: 0
    width: 6
    height: 4
  - title: Vendor Assessments by Status
    name: Vendor Assessments by Status
    model: abb
    explore: assessments
    type: looker_pie
    fields: [assessments.count, assessment_status.status]
    filters:
      assessments.type: Data-Driven Vendor Assessment,Vendor Reconnaissance Report
    sorts: [assessments.count desc]
    limit: 500
    value_labels: legend
    label_type: labPer
    inner_radius: 60
    series_colors:
      Closed: "#C2DD67"
      Scheduled: "#E57947"
      Complete: "#72D16D"
      'null': "#87857e"
    series_types: {}
    row: 49
    col: 6
    width: 9
    height: 8
  - title: Monthly Completed Vendor Assessments
    name: Monthly Completed Vendor Assessments
    model: abb
    explore: assessments
    type: looker_column
    fields: [assessments.count_of_closed_assessments, assessment_criticality.criticality,
      assessments.complete_month]
    pivots: [assessment_criticality.criticality]
    fill_fields: [assessment_criticality.criticality, assessments.complete_month]
    filters:
      assessments.type: Data-Driven Vendor Assessment,Vendor Reconnaissance Report
    sorts: [assessment_criticality.criticality, assessments.complete_month desc]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      assessment_severity.severity___null - assessment_severity.severity__sort____null - assessments.count_of_closed_assessments: "#818279"
      Low - 2 - assessments.count_of_closed_assessments: "#72D16D"
      Moderate - 1 - assessments.count_of_closed_assessments: "#FFD95F"
      High - 0 - assessments.count_of_closed_assessments: "#B32F37"
      Not Ranked - 4 - assessments.count_of_closed_assessments: "#bcbdbf"
      Low - 3 - assessments.count_of_closed_assessments: "#72D16D"
      Moderate - 2 - assessments.count_of_closed_assessments: "#FFD95F"
      High - 1 - assessments.count_of_closed_assessments: "#E57947"
      Critical - 0 - assessments.count_of_closed_assessments: "#B32F37"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#808080"
    row: 49
    col: 15
    width: 9
    height: 8
  - name: Product Assessments Overview
    type: text
    title_text: Product Assessments Overview
    row: 37
    col: 0
    width: 24
    height: 2
  - name: Vendor Assessments Overview
    type: text
    title_text: Vendor Assessments Overview
    row: 47
    col: 0
    width: 24
    height: 2
  - name: File Integrity & Authenticity Assurance
    type: text
    title_text: File Integrity & Authenticity Assurance
    row: 15
    col: 12
    width: 12
    height: 2
  - title: Total Procurements
    name: Total Procurements
    model: abb
    explore: procurements
    type: single_value
    fields: [procurements.count]
    limit: 500
    series_types: {}
    row: 33
    col: 0
    width: 6
    height: 4
  - name: Procurements Overview
    type: text
    title_text: Procurements Overview
    row: 27
    col: 0
    width: 24
    height: 2
  - title: Completed Procurements
    name: Completed Procurements
    model: abb
    explore: procurements
    type: single_value
    fields: [procurements.count_of_closed_procurements]
    limit: 500
    series_types: {}
    row: 29
    col: 0
    width: 6
    height: 4
  - title: Procurements by Status
    name: Procurements by Status
    model: abb
    explore: procurements
    type: looker_pie
    fields: [procurements.count, procurement_status.status]
    sorts: [procurements.count desc]
    limit: 500
    value_labels: legend
    label_type: labPer
    inner_radius: 60
    series_colors:
      Closed: "#72D16D"
      Open: "#B32F37"
      Contracting: "#3EB0D5"
      Planning: "#FFD95F"
      Due Diligence: "#E57947"
    series_types: {}
    listen: {}
    row: 29
    col: 6
    width: 9
    height: 8
  - title: Monthly Completed Procurements
    name: Monthly Completed Procurements
    model: abb
    explore: procurements
    type: looker_column
    fields: [procurements.count_of_closed_procurements, procurement_criticality.criticality,
      procurements.complete_month]
    pivots: [procurement_criticality.criticality]
    fill_fields: [procurement_criticality.criticality, procurements.complete_month]
    sorts: [procurement_criticality.criticality]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      Not Ranked - 4 - procurements.count_of_closed_procurements: "#91948e"
      Low - 3 - procurements.count_of_closed_procurements: "#72D16D"
      Moderate - 2 - procurements.count_of_closed_procurements: "#FFD95F"
      High - 1 - procurements.count_of_closed_procurements: "#E57947"
      Critical - 0 - procurements.count_of_closed_procurements: "#B32F37"
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: true
    show_silhouette: false
    totals_color: "#808080"
    listen: {}
    row: 29
    col: 15
    width: 9
    height: 8
  - title: Assessed High Risk Products
    name: Assessed High Risk Products
    model: abb
    explore: applications
    type: single_value
    fields: [applications.assessed, applications.count]
    filters:
      applications_criticality.criticality: Critical,High
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: true
    comparison_type: progress_percentage
    comparison_reverse_colors: false
    show_comparison_label: true
    comparison_label: high risk products
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    listen: {}
    row: 11
    col: 12
    width: 12
    height: 4
  - title: BCS Products
    name: BCS Products
    model: abb
    explore: applications
    type: single_value
    fields: [applications.count]
    filters:
      application_class.class: BCS
    limit: 500
    series_types: {}
    row: 8
    col: 12
    width: 12
    height: 3
  - title: BCS Vendors
    name: BCS Vendors
    model: abb
    explore: vendors
    type: single_value
    fields: [vendors.count]
    filters:
      vendor_business_department.business_department: "%BCS%,%EACMS%,%PACS%"
    sorts: [vendors.count desc]
    limit: 500
    series_types: {}
    listen: {}
    row: 8
    col: 0
    width: 12
    height: 3
  - title: Unresolved Monitoring Findings
    name: Unresolved Monitoring Findings
    model: abb
    explore: findings
    type: looker_pie
    fields: [findings.count, findings.finding_type]
    filters:
      resolutions.resolution: Unresolved
    sorts: [findings.count desc]
    limit: 500
    value_labels: legend
    label_type: labPer
    inner_radius: 60
    color_application:
      collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5
      palette_id: dd87bc4e-d86f-47b1-b0fd-44110dc0b469
      options:
        steps: 5
    series_colors: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    row: 17
    col: 0
    width: 12
    height: 10
  - name: Vendor Monitoring
    type: text
    title_text: Vendor Monitoring
    row: 15
    col: 0
    width: 12
    height: 2
  - name: New Procurements Over Time
    title: New Procurements Over Time
    model: abb
    explore: procurements
    type: looker_column
    fields: [procurement_workflow.__month, procurements.count]
    fill_fields: [procurement_workflow.__month]
    filters:
      procurement_workflow.workflow_step: Initiated
      procurement_workflow.step: start
    sorts: [procurement_workflow.__month desc]
    limit: 500
    column_limit: 50
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    y_axes: [{label: Count of Procurements, orientation: left, series: [{axisId: procurements.count,
            id: procurements.count, name: Procurements}], showLabels: true, showValues: true,
        unpinAxis: false, tickDensity: default, tickDensityCustom: 5, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    row: 57
    col: 0
    width: 8
    height: 6
