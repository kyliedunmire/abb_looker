view: survey_object_internal {
  sql_table_name: abb.survey_object_internal ;;

  dimension: answer_id {
    type: string
    sql: ${TABLE}."answer_id" ;;
  }

  dimension: answer_text {
    type: string
    sql: ${TABLE}."answer_text" ;;
  }

  dimension: answer_weight {
    type: number
    sql: ${TABLE}."answer_weight" ;;
  }

  dimension: bo_id {
    type: string
    sql: ${TABLE}."bo_id" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: qid {
    type: string
    sql: ${TABLE}."qid" ;;
  }

  dimension: question_id {
    type: string
    sql: ${TABLE}."question_id" ;;
  }

  dimension: question_text {
    type: string
    sql: ${TABLE}."question_text" ;;
  }

  dimension: question_weight {
    type: number
    sql: ${TABLE}."question_weight" ;;
  }

  dimension: section_index {
    type: number
    sql: ${TABLE}."section_index" ;;
  }

  dimension: section_title {
    type: string
    sql: ${TABLE}."section_title" ;;
  }

  dimension: survey_instance_id {
    type: string
    # hidden: yes
    sql: ${TABLE}."survey_instance_id" ;;
  }

  dimension: survey_template_id {
    type: string
    sql: ${TABLE}."survey_template_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [survey_instances.survey_instance_id]
  }
}
