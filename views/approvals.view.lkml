view: approvals {
  sql_table_name: mosql.approvals ;;
  drill_fields: [approval_id]

  dimension: approval_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."approval_id" ;;
  }

  dimension: _extra_props {
    hidden: yes
    type: string
    sql: ${TABLE}."_extra_props" ;;
  }

  dimension: approval_type_id {
    hidden: yes
    type: string
    sql: ${TABLE}."approval_type" ;;
  }

  dimension: approved_by_id {
    hidden: yes
    type: string
    sql: ${TABLE}."approved_by" ;;
  }

  dimension: approved_by {
    group_label: "Approved By"
    group_item_label: "Name"
    type: string
    sql: ${approval_approved_by.full_name} ;;
  }

  dimension: approved_by_email {
    group_label: "Approved By"
    group_item_label: "Email"
    type: string
    sql: ${approval_approved_by.email} ;;
  }

  dimension_group: approved {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."approved_date" ;;
  }

  dimension: approver_id {
    hidden: yes
    type: string
    sql: ${TABLE}."approver" ;;
  }

  dimension: approver {
    group_label: "Approver"
    group_item_label: "Name"
    type: string
    sql: ${approval_approver.full_name} ;;
  }

  dimension: approver_email {
    group_label: "Approver"
    group_item_label: "Email"
    type: string
    sql: ${approval_approver.email} ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}."comments" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: object_id {
    hidden: yes
    type: string
    sql: ${TABLE}."object_id" ;;
  }

  dimension_group: requested {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."requested_date" ;;
  }

  dimension: requester_id {
    hidden: yes
    type: string
    sql: ${TABLE}."requester" ;;
  }

  dimension: requester {
    group_label: "Requester"
    group_item_label: "Name"
    type: string
    sql: ${approval_requester.full_name} ;;
  }

  dimension: requester_email {
    group_label: "Requester"
    group_item_label: "Email"
    type: string
    sql: ${approval_requester.email} ;;
  }

  dimension: status_id {
    hidden: yes
    type: string
    sql: ${TABLE}."status" ;;
  }

  ####---------------------------------------------------------------------------------------------------------------------------------------------

  measure: count {
    type: count
  }
}
