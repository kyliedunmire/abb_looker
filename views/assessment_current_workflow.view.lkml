view: assessment_current_workflow {
  derived_table: {
    sql: WITH current_workflow as (SELECT *
                          FROM abb.workflow
                          WHERE workflow_pk IN (
                              SELECT max(workflow_pk)
                              FROM abb.workflow
                              GROUP BY object_id
                          ))
SELECT cw.object_id,
       cw.type,
       cw.task,
       cw.phase,
       cw.workflow_step,
       cw.step,
       cw.date,
       cw.step_due_date as due_date,
       cw.assignee,
       cw.step_user     as step_submitted_by,
       cw.days_to_complete,
       start.date as start_date,
       CASE
           WHEN cw.step <> 'complete' AND cw.step_due_date < (now())::date
               THEN TRUE
           ELSE FALSE
           END AS past_due,
      cw.status_id
FROM current_workflow cw
         LEFT JOIN (
    SELECT *
    FROM abb.workflow
    WHERE step = 'start'
) start ON cw.object_id = start.object_id AND cw.phase = start.phase AND cw.workflow_step = start.workflow_step
 ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: object_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."object_id" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension: task {
    type: string
    sql: ${TABLE}."task" ;;
  }

  dimension: phase {
    type: string
    sql: ${TABLE}."phase" ;;
  }

  dimension: current_workflow_step {
    type: string
    sql: ${TABLE}."workflow_step" ;;
  }

  dimension: step {
    type: string
    sql: ${TABLE}."step" ;;
  }

  dimension: date {
    type: date
    sql: ${TABLE}."date" ;;
  }

  dimension: due_date {
    type: date
    sql: ${TABLE}."due_date" ;;
  }

  dimension: assignee_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assignee" ;;
  }

  dimension: step_owner {
    type: string
    sql: ${current_workflow_assignee.full_name} ;;
  }

  dimension: step_submitted_by {
    type: string
    sql: ${TABLE}."step_submitted_by" ;;
  }

  dimension: days_to_complete {
    type: number
    sql: ${TABLE}."days_to_complete" ;;
  }

  dimension: start_date {
    type: date
    sql: ${TABLE}."start_date" ;;
  }

  dimension: past_due {
    type: yesno
    sql: ${TABLE}."past_due" ;;
  }

  dimension: status_id {
    hidden: yes
    type: string
    sql: ${TABLE}.status_id ;;
  }

  dimension: days_past_due {
    type: number
    sql: CASE
          WHEN ${step} <> 'complete' AND ${past_due} IS TRUE
          THEN ${due_date} - ${start_date}
          ELSE NULL
          END;;
  }

  dimension: platform_id {
    type: string
    sql: ${assessments.platform_id} ;;
  }

  dimension: name {
    type: string
    sql: ${assessments.name} ;;
  }

  dimension: coming_due_past_due {
    label: "Coming Due/Past Due"
    type: string
    sql: CASE
         WHEN ${step} = 'complete'
         THEN 'Complete'
         WHEN extract(days from (${due_date} - now())) < 0
         THEN 'Past due'
         WHEN extract(days from (${due_date} - now())) < 3
         THEN 'Coming due'
         ELSE 'No escalation required'
         END;; #There are 2 assessment finding status documents, bringing in the back end ID of both just in case
    description: "Generalizing all incomplete Assessments as either past due or coming due within 2 days"
  }

  set: detail {
    fields: [
      platform_id,
      assessments.name,
      current_workflow_step,
      step_owner,
      start_date,
      step,
      due_date,
      past_due
    ]
  }


}
