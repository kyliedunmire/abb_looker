view: solutions {
  sql_table_name: abb.solutions ;;
  drill_fields: [solution_id]

  dimension: solution_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}."solution_id" ;;
  }

  dimension: assessment_id {
    type: string
    hidden: yes
    sql: ${TABLE}."assessment_id" ;;
  }

  dimension: assigned_to_id {
    hidden: yes
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: evidence_link {
    type: string
    sql: ${TABLE}."evidence_link" ;;
  }

  dimension: is_submitted {
    type: yesno
    sql: ${TABLE}."is_submitted" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: severity {
    type: string
    sql: ${TABLE}."severity" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."solution_name" ;;
    html: <a href="https://fis-mtn-looker.fortressis.com/cyberSecurity/solutions/details/{{ solutions.solution_id }}" target="_blank">{{value}}</a> ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}."status" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension: class_id {
    hidden: yes
    type: string
    sql: ${TABLE}.class_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      solution_id,
      name,
      assessments.assessment_id,
      assessments.assessment_name,
      assessments.count,
      vulnerability_solutions.count
    ]
  }
}
