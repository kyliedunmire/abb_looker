view: application_detected_vulnerabilities {
  sql_table_name: abb.application_detected_vulnerabilities ;;

  dimension: application_id {
    type: string
    hidden: yes
    sql: ${TABLE}."application_id" ;;
  }

  dimension: detected_vulnerability_id {
    hidden: yes
    type: string
    sql: ${TABLE}."detected_vulnerability_id" ;;
  }

  measure: count {
    type: count
    drill_fields: [applications.application_name, applications.application_id]
  }
}
