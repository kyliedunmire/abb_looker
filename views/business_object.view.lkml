view: business_object {
  sql_table_name: abb.business_object ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}."id" ;;
  }

  dimension: _extra_props {
    type: string
    sql: ${TABLE}."_extra_props" ;;
  }

  dimension: access_complexity {
    type: string
    sql: ${TABLE}."access_complexity" ;;
  }

  dimension: accountability_loss_impact_factor {
    type: string
    sql: ${TABLE}."accountability_loss_impact_factor" ;;
  }

  dimension: address {
    type: string
    sql: ${TABLE}."address" ;;
  }

  dimension: asset_suite_contract_id {
    type: string
    sql: ${TABLE}."asset_suite_contract_id" ;;
  }

  dimension: asset_suite_vendor_id {
    type: string
    sql: ${TABLE}."asset_suite_vendor_id" ;;
  }

  dimension: assigned_org {
    type: string
    sql: ${TABLE}."assigned_org" ;;
  }

  dimension: assigned_to {
    type: string
    sql: ${TABLE}."assigned_to" ;;
  }

  dimension: assignedto_conditional {
    type: string
    sql: ${TABLE}."assignedto_conditional" ;;
  }

  dimension: attack_vector {
    type: string
    sql: ${TABLE}."attack_vector" ;;
  }

  dimension: authentication {
    type: string
    sql: ${TABLE}."authentication" ;;
  }

  dimension: availability_impact {
    type: string
    sql: ${TABLE}."availability_impact" ;;
  }

  dimension: availability_loss_impact_factor {
    type: string
    sql: ${TABLE}."availability_loss_impact_factor" ;;
  }

  dimension: awareness {
    type: string
    sql: ${TABLE}."awareness" ;;
  }

  dimension: business_department {
    type: string
    sql: ${TABLE}."business_department" ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}."category" ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}."city" ;;
  }

  dimension: class {
    type: string
    sql: ${TABLE}."class" ;;
  }

  dimension_group: closed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."closed_date" ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}."code" ;;
  }

  dimension: confidentiality_impact {
    type: string
    sql: ${TABLE}."confidentiality_impact" ;;
  }

  dimension: confidentiality_loss_impact_factor {
    type: string
    sql: ${TABLE}."confidentiality_loss_impact_factor" ;;
  }

  dimension: contract_number {
    type: string
    sql: ${TABLE}."contract_number" ;;
  }

  dimension: coso_impact {
    type: string
    sql: ${TABLE}."coso_impact" ;;
  }

  dimension: coso_likelihood {
    type: string
    sql: ${TABLE}."coso_likelihood" ;;
  }

  dimension: coso_speed_and_onset {
    type: string
    sql: ${TABLE}."coso_speed_and_onset" ;;
  }

  dimension: coso_vulnerability {
    type: string
    sql: ${TABLE}."coso_vulnerability" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."created_at" ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}."created_by" ;;
  }

  dimension: created_on_behalf_of {
    type: string
    sql: ${TABLE}."created_on_behalf_of" ;;
  }

  dimension: credential_status {
    type: string
    sql: ${TABLE}."credential_status" ;;
  }

  dimension: cvss {
    type: string
    sql: ${TABLE}."cvss" ;;
  }

  dimension: cwe {
    type: string
    sql: ${TABLE}."cwe" ;;
  }

  dimension: cybersecurity_framework_category {
    type: string
    sql: ${TABLE}."cybersecurity_framework_category" ;;
  }

  dimension_group: date_reported {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."date_reported_date" ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}."description" ;;
  }

  dimension: determination_q1 {
    type: string
    sql: ${TABLE}."determination_q1" ;;
  }

  dimension: determination_q2 {
    type: string
    sql: ${TABLE}."determination_q2" ;;
  }

  dimension: determination_q3 {
    type: string
    sql: ${TABLE}."determination_q3" ;;
  }

  dimension: determination_q4 {
    type: string
    sql: ${TABLE}."determination_q4" ;;
  }

  dimension: determination_q5 {
    type: string
    sql: ${TABLE}."determination_q5" ;;
  }

  dimension: disposition_required {
    type: string
    sql: ${TABLE}."disposition_required" ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."due_date" ;;
  }

  dimension: ease_of_discovery {
    type: string
    sql: ${TABLE}."ease_of_discovery" ;;
  }

  dimension: ease_of_exploit {
    type: string
    sql: ${TABLE}."ease_of_exploit" ;;
  }

  dimension: enterprise_architect_email {
    type: string
    sql: ${TABLE}."enterprise_architect_email" ;;
  }

  dimension: enterprise_architect_name {
    type: string
    sql: ${TABLE}."enterprise_architect_name" ;;
  }

  dimension_group: est_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."est_completed_date" ;;
  }

  dimension: evidence_link {
    type: string
    sql: ${TABLE}."evidence_link" ;;
  }

  dimension: financial_damage_impact_factor {
    type: string
    sql: ${TABLE}."financial_damage_impact_factor" ;;
  }

  dimension: impact {
    type: string
    sql: ${TABLE}."impact" ;;
  }

  dimension: integrity_impact {
    type: string
    sql: ${TABLE}."integrity_impact" ;;
  }

  dimension: integrity_loss_impact_factor {
    type: string
    sql: ${TABLE}."integrity_loss_impact_factor" ;;
  }

  dimension: intrustion_detection {
    type: string
    sql: ${TABLE}."intrustion_detection" ;;
  }

  dimension: ip_addresses {
    type: string
    sql: ${TABLE}."ip_addresses" ;;
  }

  dimension: is_closed {
    type: string
    sql: ${TABLE}."is_closed" ;;
  }

  dimension: is_submitted {
    type: string
    sql: ${TABLE}."is_submitted" ;;
  }

  dimension: iso_mapping {
    type: string
    sql: ${TABLE}."iso_mapping" ;;
  }

  dimension: it_demand_email {
    type: string
    sql: ${TABLE}."it_demand_email" ;;
  }

  dimension: it_demand_name {
    type: string
    sql: ${TABLE}."it_demand_name" ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}."location" ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}."name" ;;
  }

  dimension: non_compliance_impact_factor {
    type: string
    sql: ${TABLE}."non_compliance_impact_factor" ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}."notes" ;;
  }

  dimension: os_family {
    type: string
    sql: ${TABLE}."os_family" ;;
  }

  dimension: os_vendor {
    type: string
    sql: ${TABLE}."os_vendor" ;;
  }

  dimension: os_version {
    type: string
    sql: ${TABLE}."os_version" ;;
  }

  dimension: other_stakeholder_email {
    type: string
    sql: ${TABLE}."other_stakeholder_email" ;;
  }

  dimension: other_stakeholder_name {
    type: string
    sql: ${TABLE}."other_stakeholder_name" ;;
  }

  dimension: owasp {
    type: string
    sql: ${TABLE}."owasp" ;;
  }

  dimension: patch_applicability {
    type: string
    sql: ${TABLE}."patch_applicability" ;;
  }

  dimension: platform_id {
    type: string
    sql: ${TABLE}."platform_id" ;;
  }

  dimension: postal_code {
    type: string
    sql: ${TABLE}."postal_code" ;;
  }

  dimension: prior_status {
    type: string
    sql: ${TABLE}."prior_status" ;;
  }

  dimension: privacy_violation_impact_factor {
    type: string
    sql: ${TABLE}."privacy_violation_impact_factor" ;;
  }

  dimension: procurement_contact_email {
    type: string
    sql: ${TABLE}."procurement_contact_email" ;;
  }

  dimension: procurement_contact_name {
    type: string
    sql: ${TABLE}."procurement_contact_name" ;;
  }

  dimension: project_manager_email {
    type: string
    sql: ${TABLE}."project_manager_email" ;;
  }

  dimension: project_manager_name {
    type: string
    sql: ${TABLE}."project_manager_name" ;;
  }

  dimension: recommendation {
    type: string
    sql: ${TABLE}."recommendation" ;;
  }

  dimension: related_applications {
    type: string
    sql: ${TABLE}."related_applications" ;;
  }

  dimension: related_assessment_findings {
    type: string
    sql: ${TABLE}."related_assessment_findings" ;;
  }

  dimension: related_assessments {
    type: string
    sql: ${TABLE}."related_assessments" ;;
  }

  dimension: related_assets {
    type: string
    sql: ${TABLE}."related_assets" ;;
  }

  dimension: related_detected_vulnerabilities {
    type: string
    sql: ${TABLE}."related_detected_vulnerabilities" ;;
  }

  dimension: related_locations {
    type: string
    sql: ${TABLE}."related_locations" ;;
  }

  dimension: related_risk_outcomes {
    type: string
    sql: ${TABLE}."related_risk_outcomes" ;;
  }

  dimension: related_services {
    type: string
    sql: ${TABLE}."related_services" ;;
  }

  dimension: related_solutions {
    type: string
    sql: ${TABLE}."related_solutions" ;;
  }

  dimension: related_systems {
    type: string
    sql: ${TABLE}."related_systems" ;;
  }

  dimension: related_threats {
    type: string
    sql: ${TABLE}."related_threats" ;;
  }

  dimension: related_vendors {
    type: string
    sql: ${TABLE}."related_vendors" ;;
  }

  dimension: related_vulnerabilities {
    type: string
    sql: ${TABLE}."related_vulnerabilities" ;;
  }

  dimension_group: reopened {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."reopened_date" ;;
  }

  dimension_group: reported {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."reported_date" ;;
  }

  dimension: reputation_damage_impact_factor {
    type: string
    sql: ${TABLE}."reputation_damage_impact_factor" ;;
  }

  dimension: resolution {
    type: string
    sql: ${TABLE}."resolution" ;;
  }

  dimension: safety_impact_factor {
    type: string
    sql: ${TABLE}."safety_impact_factor" ;;
  }

  dimension: security_ambassador_email {
    type: string
    sql: ${TABLE}."security_ambassador_email" ;;
  }

  dimension: security_ambassador_name {
    type: string
    sql: ${TABLE}."security_ambassador_name" ;;
  }

  dimension: severity {
    type: string
    sql: ${TABLE}."severity" ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}."state" ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}."status" ;;
  }

  dimension: sub_type {
    type: string
    sql: ${TABLE}."sub_type" ;;
  }

  dimension: submitted_by {
    type: string
    sql: ${TABLE}."submitted_by" ;;
  }

  dimension: submitted_by_email {
    type: string
    sql: ${TABLE}."submitted_by_email" ;;
  }

  dimension_group: submitted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."submitted_date" ;;
  }

  dimension: subsidiary {
    type: string
    sql: ${TABLE}."subsidiary" ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}."tags" ;;
  }

  dimension: technical_department {
    type: string
    sql: ${TABLE}."technical_department" ;;
  }

  dimension: tps {
    type: string
    sql: ${TABLE}."tps" ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}."type" ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."updated_at" ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}."updated_by" ;;
  }

  dimension: vendor_primary_contact_email {
    type: string
    sql: ${TABLE}."vendor_primary_contact_email" ;;
  }

  dimension: vendor_primary_contact_name {
    type: string
    sql: ${TABLE}."vendor_primary_contact_name" ;;
  }

  dimension: vendor_primary_contact_phone {
    type: string
    sql: ${TABLE}."vendor_primary_contact_phone" ;;
  }

  dimension: version_information {
    type: string
    sql: ${TABLE}."version_information" ;;
  }

  dimension: workflow {
    type: string
    sql: ${TABLE}."workflow" ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      procurement_contact_name,
      enterprise_architect_name,
      it_demand_name,
      project_manager_name,
      security_ambassador_name,
      other_stakeholder_name,
      vendor_primary_contact_name
    ]
  }
}
